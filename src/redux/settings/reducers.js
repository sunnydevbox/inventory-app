// import store from 'store'
import actions from './actions'

const STORED_SETTINGS = (storedSettings) => {
  // const settings = {}
  // Object.keys(storedSettings).forEach((key) => {
  //   const item = store.get(`app.settings.${key}`)
  //   settings[key] = typeof item !== 'undefined' ? item : storedSettings[key]
  // })
  console.log(storedSettings)
  return storedSettings
  // return settings
}

export const initialState = {
  ...STORED_SETTINGS({
    // Read docs for available values: https://docs.visualbuilder.cloud
    authProvider: 'jwt',
    appName: 'Sunnydevbox',
    logo: 'Your Business Here :)',
    version: 'fluent',
    theme: 'default',
    locale: 'en-US',
    isSidebarOpen: false,
    isSupportChatOpen: false,
    isMobileView: false,
    isMobileMenuOpen: false,
    isMenuCollapsed: false,
    isPreselectedOpen: false,
    preselectedVariant: 'default',
    menuLayoutType: 'top',
    routerAnimation: 'none',
    menuColor: 'white',
    authPagesColor: 'gray',
    isAuthTopbar: true,
    primaryColor: '#4b7cf3',
    leftMenuWidth: 256,
    isMenuUnfixed: false,
    isMenuShadow: false,
    isTopbarFixed: true,
    isTopbarSeparated: false,
    isGrayTopbar: false,
    isContentMaxWidth: false,
    isAppMaxWidth: false,
    isGrayBackground: true,
    isCardShadow: true,
    isSquaredBorders: false,
    isBorderless: false,
    layoutMenu: 'classic',
    layoutTopbar: 'v1',
    layoutBreadcrumbs: 'noBreadcrumbs',
    layoutFooter: 'v1',
    flyoutMenuType: 'classic',
    flyoutMenuColor: 'blue',
  }),
}

export default function settingsReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE:
      return { ...state, ...action.payload }
    default:
      return state
  }
}
