import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

// import featureSupplier from 'pages/settings/suppliers/redux/reducers'
// import featureCategory from 'pages/settings/category/redux/reducers'
// import featureCustomer from 'pages/settings/customers/redux/reducers'
// import featurePurchaseOrder from 'pages/purchase-orders/redux/reducers'
// import featureProduct from 'pages/products/redux/reducers'
// import featureLocation from 'pages/settings/warehouses/redux/reducers'
// import featureMetric from 'pages/settings/units/redux/reducers'
import featureUser from 'pages/settings/users/redux/reducers'
// import featurePaymentTerm from 'pages/settings/payment-terms/redux/reducers'
import featureBread from 'components/bread/redux/reducers'

import user from './user/reducers'
import menu from './menu/reducers'
import settings from './settings/reducers'

export default (history) =>
  combineReducers({
    router: connectRouter(history),
    user,
    menu,
    settings,

    // featureSupplier,
    // featureCategory,
    // featureCustomer,
    // featurePurchaseOrder,
    // featureProduct,
    // featureLocation,
    // featureMetric,
    featureUser,
    // featurePaymentTerm,
    featureBread,
  })
