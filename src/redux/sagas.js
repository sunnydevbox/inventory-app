import { all } from 'redux-saga/effects'

// import featureSupplier from 'pages/settings/suppliers/redux/sagas'
// import featureCategory from 'pages/settings/category/redux/sagas'
// import featureCustomer from 'pages/settings/customers/redux/sagas'
import featureBread from 'components/bread/redux/sagas'
// import featurePurchaseOrder from 'pages/purchase-orders/redux/sagas'
// import featureProduct from 'pages/products/redux/sagas'
// import featureLocation from 'pages/settings/warehouses/redux/sagas'
// import featureMetric from 'pages/settings/units/redux/sagas'
import featureUser from 'pages/settings/users/redux/sagas'
// import featurePaymentTerm from 'pages/settings/payment-terms/redux/sagas'

import user from './user/sagas'
import menu from './menu/sagas'
import settings from './settings/sagas'

export default function* rootSaga() {
  yield all([
    user(), menu(), settings(),
  
    // featureSupplier(),
    // featureCategory(),
    // featureCustomer(),
    // featurePurchaseOrder(),
    // featureProduct(),
    // featureLocation(),
    // featureMetric(),
    featureUser(),
    // featurePaymentTerm(),

    featureBread(),
  ])
}
