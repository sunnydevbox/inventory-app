const actions = {
  SET_STATE: 'user/SET_STATE',
  LOGIN: 'user/LOGIN',
  REGISTER: 'user/REGISTER',
  LOAD_CURRENT_ACCOUNT: 'user/LOAD_CURRENT_ACCOUNT',

  LOGOUT: 'user/LOGOUT',
  ME: 'user/ME',
  SET_ACCOUNT: 'user/SET_ACCOUNT',
}

export default actions
