import { all, takeEvery, put, call, select } from 'redux-saga/effects'
import { notification } from 'antd'
import { history } from 'index'
import {
  login,
  currentAccount,
  setAccount,
  logout,
  me,
  setToken
} from 'services/UserService'
// import * as u from 'services/UserService'
import * as jwt from 'services/jwt'
import actions from './actions'
import { initialState } from './reducers'


const mapAuthProviders = {
  jwt: {
    login: jwt.login,
    register: jwt.register,
    currentAccount: jwt.currentAccount,
    logout: jwt.logout,
  },
}

export function* LOGIN({ payload }) {
  const { email, password } = payload
  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: true,
      error: null,
    },
  })
  // const { authProvider: autProviderName } = yield select((state) => state.settings)


  const response = yield call(login, email, password)

  if (response && response.t) {
    yield call(setToken, response.t)
    // const u = yield call(me, response.t)

    yield put({
      type: 'user/ME',
    })
    yield put({
      type: 'user/SET_STATE',
      payload: {
        loading: false,
      },
    })
  } else {
    yield put({
      type: 'user/SET_STATE',
      payload: {
        loading: false,
      },
    })

    if (response) {
      let errorMessage = null

      if (response.data.error) {
        errorMessage = response.data.error

        if (response.data.error === 'Unauthorized') {
          errorMessage = 'Your email and password do not match'
        }
      } else if (response.data.message) {
        errorMessage = response.data.message
        if (response.data.message === 'invalid_credentials') {
          errorMessage = 'Your email and password do not match'
        }
      }

      yield put({
        type: 'user/SET_STATE',
        payload: {
          error: errorMessage
        },
      })

      // if (
      //   response.data.message === 'invalid_credentials'
      //   || response.data.message === 'Unauthorized'
      //   || response.data.error === 'Unauthorized'
      // ) {
      //   notification.warning({
      //     message: 'Authentication Failed',
      //     description: 'Email or password is invalid!',
      //   })
      // }
    }
  }

  // if (success) {
  //   yield put({
  //     type: 'user/LOAD_CURRENT_ACCOUNT',
  //   })
  //   yield history.push('/')
  //   notification.success({
  //     message: 'Logged In',
  //     description: 'You have successfully logged in!',
  //   })
  // }

  // if (!success) {
  //   yield put({
  //     type: 'user/SET_STATE',
  //     payload: {
  //       loading: false,
  //     },
  //   })
  // }
}

export function* REGISTER({ payload }) {
  const { email, password, name } = payload
  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: true,
    },
  })
  const { authProvider } = yield select((state) => state.settings)
  const success = yield call(mapAuthProviders[authProvider].register, email, password, name)
  if (success) {
    yield put({
      type: 'user/LOAD_CURRENT_ACCOUNT',
    })
    yield history.push('/')
    notification.success({
      message: 'Succesful Registered',
      description: 'You have successfully registered!',
    })
  }
  if (!success) {
    yield put({
      type: 'user/SET_STATE',
      payload: {
        loading: false,
      },
    })
  }
}

export function* LOAD_CURRENT_ACCOUNT() {
  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: true,
      error: null,
    },
  })
  const response = yield call(currentAccount)

  if (response && response.id) {
    const { avatar, email, firstName, id, lastName, permissions, roles } = response
    yield put({
      type: 'user/SET_STATE',
      payload: {
        id,
        name: `${firstName} ${lastName}`,
        firstName,
        lastName,
        email,
        avatar,
        permissions,
        roles,
        authorized: true,
      },
    })
  } else {
    // CLEAR ALL
    yield call(logout)
  }
  yield put({
    type: 'user/SET_STATE',
    payload: {
      loading: false,
    },
  })
}

export function* LOGOUT() {
  // alert('LOGOUT')
  yield call(logout)
  yield put({
    type: 'user/SET_STATE',
    payload: initialState,
  })
}

export function* ME() {
  const user = yield call(me)

  const payload = {
    id: user.data.id,
    firstName: user.data.first_name,
    lastName: user.data.last_name,
    roles: user.data.roles,
    permissions: user.data.permissions,
    authorized: true,
    // avatar: user.picture,
  }

  if (user.data.email) {
    payload.email = user.data.email
  }

  yield put({
    type: 'user/SET_ACCOUNT',
    payload,
  })

  // yield put({
  //   type: 'user/LOAD_CURRENT_ACCOUNT',
  // })
}

export function* SET_ACCOUNT(payload) {
  yield call(setAccount, payload.payload)

  yield put({
    type: 'user/SET_STATE',
    payload: payload.payload,
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.LOGIN, LOGIN),
    takeEvery(actions.REGISTER, REGISTER),
    takeEvery(actions.LOAD_CURRENT_ACCOUNT, LOAD_CURRENT_ACCOUNT),
    takeEvery(actions.LOGOUT, LOGOUT),

    takeEvery(actions.ME, ME),
    takeEvery(actions.SET_ACCOUNT, SET_ACCOUNT),
    LOAD_CURRENT_ACCOUNT(), // run once on app load to check user auth
  ])
}
