import React from 'react'
import ReactDOM from 'react-dom'
import {
  // createHashHistory,
  createBrowserHistory,
} from 'history'
import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
// import { logger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import { routerMiddleware } from 'connected-react-router'
// import * as localforage from 'localforage'
// import { offline } from 'redux-offline'
// import offlineConfig from 'redux-offline/lib/defaults'

import StylesLoader from './stylesLoader'
import reducers from './redux/reducers'
import sagas from './redux/sagas'
import Localization from './localization'
import Router from './router'
import * as serviceWorker from './serviceWorker'

// mocking api
import 'services/axios/fakeApi'

// middlewared
// let history = createHashHistory()
const history = createBrowserHistory()
const sagaMiddleware = createSagaMiddleware()
const routeMiddleware = routerMiddleware(history)
const middlewares = [sagaMiddleware, routeMiddleware]
// if (process.env.NODE_ENV === 'development') {
//   middlewares.push(logger)
// }

// FOR OFFLINE MODE
// offlineConfig.persistOptions = { storage: localforage }; // store offline data in indexedDB

const store = createStore(
  reducers(history),
  compose(
    applyMiddleware(...middlewares),
    // offline()
  ),
)
sagaMiddleware.run(sagas)

if (process.env.REACT_APP_ENV === 'production') {
  console.log = () => {}
  console.error = () => {}
  console.debug = () => {}
}

ReactDOM.render(
  <Provider store={store}>
    <StylesLoader>
      <Localization>
        <Router history={history} />
      </Localization>
    </StylesLoader>
  </Provider>,
  document.getElementById('root'),
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
export { store, history }
