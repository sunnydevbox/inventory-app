import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { notification } from 'antd'
import Forbidden from 'pages/auth/forbidden'

@connect(({ user }) => ({ user }))
class Authorize extends React.Component {
  render() {
    const {
      user: {
        // role,
        permissions: userPermissions,
      },
    } = this.props // current user role

    const { children, redirect = false, to = '/404', permissions = [] } = this.props

    // const authorized = permissions.includes(role)
    let allowed = false
    if (userPermissions) {
      allowed = permissions.some(r => userPermissions.indexOf(r) >= 0)
    }
    // console.log(permissions, userPermissions)
    // console.log('authorized',found, authorized, user, roles, role, permissions)
    const AuthorizedChildren = () => {
      // if user not equal needed role and if component is a page - make redirect to needed route
      if (!allowed && redirect) {
        notification.error({
          message: 'Unauthorized Access',
          description: 'You have no rights to access this page!',
        })
        return <Redirect to={to} />
      }

      if (!allowed && !redirect) {
        return <Forbidden />
      }
      // if user not authorized return null to component
      if (!allowed) {
        return null
      }
      // if access is successful render children
      return <div>{children}</div>
    }
    return AuthorizedChildren()
  }
}

export default Authorize
