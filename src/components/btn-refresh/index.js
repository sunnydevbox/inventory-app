import React from 'react'
import { Button } from 'antd'
import { SyncOutlined } from '@ant-design/icons'

const RefreshButtonComponent = ({ label, callback, loading, ...props }) => {
  return (
    <>
      <Button
        icon={<SyncOutlined spin={loading} />}
        onClick={callback}
        type="primary"
        shape="round"
        {...props}
      >
        {label}
      </Button>
    </>
  )
}

export default RefreshButtonComponent
