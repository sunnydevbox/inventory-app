import React, { useEffect, useCallback, useState } from 'react'
import { Helmet } from 'react-helmet'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import BrowseComponent from 'components/bread/browse'
import DrawerComponent from 'components/bread/content'
import { STORE_PREFIX } from './config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const ContentManagerComponent = ({
  [STORE_PREFIX]: { API_SERVICE },
  dispatch,
  ...props
}) => {
  const [currentKey, setCurrentKey] = useState(null)
  const [currentService, setCurrentService] = useState(null)
  const [firstLoad, setFirstLoad] = useState(true)

  const resetState = useCallback(resetting => dispatch({ type: `${STORE_PREFIX}/RESET_STATE`, resetting }), [dispatch])
  const setApiService = useCallback(api => dispatch({ type: `${STORE_PREFIX}/SET_API_SERVICE`, API_SERVICE: api}), [dispatch])

  useEffect(() => {
    const {
      location: {pathname}, 
      config } = props
   
    if (!currentKey) {
      resetState(true)
      setCurrentKey(pathname)
    } 
    

    if (firstLoad) {
      setFirstLoad(false)
      resetState(true)
      if (!currentService) {
        setCurrentService(config.API_SERVICE)
        setApiService(config.API_SERVICE)
      }
    }

    

    if (
      (currentService && (API_SERVICE && !API_SERVICE.axiosAPI))
      || (currentService && !API_SERVICE)
    ) {
      // console.log('API_SERVICE', 
      // currentService, 
      //   API_SERVICE, 
      //   // API_SERVICE.axiosAPI
      //   )
      setApiService(currentService)
    }

    props.history.listen((
      location,
      action
    ) => {
      // console.log(location)
      if (action === 'PUSH') {
        resetState(true)
      }
    })
  }, [
    currentService,
    firstLoad,
    currentKey,
    API_SERVICE,
    props,
    resetState,
    setCurrentService,
    setApiService,
    setCurrentKey
  ])

  return (
    <div>
      <Helmet title={props.config.LABEL_PLURAL} />
      {(!firstLoad)? 
        (
          <>
            <DrawerComponent
              content={props.content}
              {...props}
            />

            <section className="card">
              <div className="card-body">
                <BrowseComponent
                  API_SERVICE={currentService}
                  {...props}
                />
              </div>
            </section>
          </>
        )
        : null
      }
      
    </div>
  )
}


export default withRouter(connect(mapStateToProps)(ContentManagerComponent))