import React, { useEffect, useCallback } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import TableComponent from './table'
import TableHeaderComponent from './table-header'
import { STORE_PREFIX } from '../config'
import './styles.scss'


const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const FeatureBrowseComponent = ({
  config,
  dispatch,
  ...props
}) => {
  const setApiService = useCallback(api => dispatch({ type: `${STORE_PREFIX}/SET_API_SERVICE`, API_SERVICE: api}), [dispatch])

  useEffect(() => { 
    const { API_SERVICE } = props[STORE_PREFIX]

    if (!API_SERVICE) {
      setApiService(props.API_SERVICE)
    }
  }, [
    props,
    setApiService
  ])
  
  const getHeaderActions = () => {
    let a = {}
    if (props.headerActions) {
      a = props.headerActions
    }

    return a
  }

  return (
    <>
      <TableHeaderComponent
        data={{ title: config.LABEL_PLURAL }}
        actions={getHeaderActions()}
        {...props}
      />
      <TableComponent {...props} />
    </>
  )
}

export default withRouter(connect(mapStateToProps)(FeatureBrowseComponent))
