import React, { useCallback, useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Table, Modal } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons';
import parser from 'html-react-parser'
import { STORE_PREFIX } from '../../config'
import './styles.scss'

const { confirm } = Modal;

const TableComponent = ({ 
  dispatch,
  browse,
  ...props
}) => {
  const [firstLoad, setFirstLoad] = useState(true)
  const [dList, setDataList] = useState([])
  const [cols, setColumns] = useState([])
  const [rowActions, setRowActions] = useState(null)
  const [isLoading, setListLoading] = useState(true)
  const [pag, setPagination] = useState(null)
  const [actionsAttached, setActionsAttached] = useState(false)

  const getList = useCallback(() => { dispatch({ type: `${STORE_PREFIX}/API_GET_LIST` }) }, [dispatch])
  const toggleDrawer = useCallback(() => { dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }) }, [dispatch])
  const setFilter = useCallback(filters => { dispatch({ type: `${STORE_PREFIX}/SET_FILTER`, filters }) }, [dispatch])
  const setRecord = useCallback(id => { dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id }) }, [dispatch])
  const setContentViewMode = useCallback(viewMode => { dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }) }, [dispatch])
  const deleteRecord = useCallback(id => { dispatch({ type: `${STORE_PREFIX}/API_DELETE_RECORD`, id }) }, [dispatch])

  useEffect(() => {
    const { 
        listLoading,
        dataList,
        pagination,
    } = props[STORE_PREFIX]

    setListLoading(listLoading)

    if (firstLoad) {
      setFirstLoad(false)
      getList()
    }

    if(!pag) {
      setPagination(pagination)
    }

    setDataList(dataList)
    
    if (!cols.length) {
      // BACKWARD COMPATIBLITY
      if (browse && browse.columns) {
        setColumns(browse.columns)
      } else {
        // SHOULD BE DEPRECATED
        setColumns(props.columns)
      }
    }

    if (!rowActions) {
      setRowActions({ ...props.rowActions }, { ...TableComponent.defaultProps.rowActions })
    }

  }, [
    pag,
    cols,
    firstLoad,
    rowActions,
    browse,
    getList,
    setListLoading,
    // setApiService,
    props
  ])

  const getEditAction = (record) => {
    
    if (browse && browse.actions) {
      const { edit } = browse.actions
      if (edit.show(record)) {
        return (
          <span>
            <a
              className="btn btn-sm btn-light mr-2"
              href="#"
              onClick={(e) => {
                setContentViewMode('edit')
                setRecord(record.id)
                toggleDrawer()
                e.preventDefault()
              }}
            >
              <i className="fe fe-edit mr-2" />Edit
            </a>
          </span>
        )
      }
    } else if (rowActions && rowActions.edit && rowActions.edit.enabled) {
      return (
        <span>
          <a
            className="btn btn-sm btn-light mr-2"
            href="#"
            onClick={(e) => {
              setContentViewMode('edit')
              setRecord(record.id)
              toggleDrawer()
              e.preventDefault()
            }}
          >
            <i className="fe fe-edit mr-2" />Edit
          </a>
        </span>
      )
    }

    return null
  }

  const getViewAction = (record) => {
    if (browse && browse.actions) {
      const { view } = browse.actions
      if (view.show(record)) {
        return (
          <span>
            <a
              className="btn btn-sm btn-light mr-2"
              href="#"
              onClick={(e) => {
                setRecord(record.id)
                setContentViewMode('view')
                toggleDrawer()
                e.preventDefault()
              }}
            >
              <i className="fe fe-eye mr-2" />View
            </a>
          </span>
        )
      }
    } else if (rowActions && rowActions.view && rowActions.view.enabled) {
      return (
        <span>
          <a
            className="btn btn-sm btn-light mr-2"
            href="#"
            onClick={(e) => {
              setRecord(record.id)
              setContentViewMode('view')
              toggleDrawer()
              e.preventDefault()
            }}
          >
            <i className="fe fe-eye mr-2" />View
          </a>
        </span>
      )
    }

    return null
  }

  const getDeleteAction = (record) => {
    if (browse && browse.actions) {
      const { destroy } = browse.actions
      if (destroy.show(record)) {
        return (
          <span>
            <a
              className="btn btn-sm btn-warning mr-2"
              href="#"
              onClick={() => {
                confirm({
                  title: `You are about to delete this record"?`,
                  icon: <ExclamationCircleOutlined />,
                  content: parser('This action cannot be reversed.<br/><br/>Are you sure you want to continue?'),
                  okText: 'Yes',
                  cancelText: 'No',
                  onOk() {
                    deleteRecord(record.id)
                  },
                  onCancel() {
                    // console.log('Cancel');
                  },
                });
              }}
            >
              <i className="fe fe-trash mr-2" />Remove
            </a>
          </span>
        )
      }
    } else if (rowActions && rowActions.delete && rowActions.delete.enabled) {
      return (
        <span>
          <a
            className="btn btn-sm btn-warning mr-2"
            href="#"
            onClick={() => {
              confirm({
                title: `Do you want to delete "${record.name}"?`,
                icon: <ExclamationCircleOutlined />,
                content: 'This action cannot be reversed',
                onOk() {
                  deleteRecord(record.id)
                },
                onCancel() {
                  // console.log('Cancel');
                },
              });
            }}
          >
            <i className="fe fe-trash mr-2" />Remove
          </a>
        </span>
      )
    }

    return null
  }

  const getColumns = () => {
    const c = cols    
    if (!actionsAttached) {
      if (c.length) {
        setActionsAttached(true)
        c.push({
          title: 'Action',
          key: 'action',
          render: (record) => {
            return (
              <>
                {getViewAction(record)}
                {getEditAction(record)}
                {getDeleteAction(record)}
              </>

            )
          }
        })
      }
    }

    return c
  }

  return (
    <>
      <Table
        rowClassName="text-small"
        loading={isLoading}
        dataSource={dList}
        columns={getColumns()}
        rowKey={row => row.id}
        pagination={{
          showTotal: total => `Total ${total} items`,
          total: props[STORE_PREFIX].pagination.total,
          defaultPageSize: props[STORE_PREFIX].pagination.per_page,
          onChange: (page, pageSize) => {
            setFilter({
              page,
              limit: pageSize,
            })
          },
          position: ['bottomCenter'],
        }}
      />
    </>
  )
}

TableComponent.defaultProps = {
  rowActions: {
    view: {
      enabled: true,
    },
    edit: {
      enabled: true
    },
    delete: {
      enabled: true,
    }
  }
}


export default connect()(TableComponent)
