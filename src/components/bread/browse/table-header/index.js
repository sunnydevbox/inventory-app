import React, { useEffect, useCallback, useState } from 'react'
import { connect } from 'react-redux'
import { Tooltip } from 'antd'
import { SyncOutlined } from '@ant-design/icons'
import { STORE_PREFIX } from '../../config'
import './styles.scss'

const Header = ({
  dispatch,
  data,
  action,
  ...props
}) => {
  const [actions, setActions] = useState(null)

  const getList = useCallback(() => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST` }), [dispatch])
  const toggleDrawer = useCallback(() => { dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }) }, [dispatch])
  const setContentViewMode = useCallback(viewMode => { dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }) }, [dispatch])
  const setRecord = useCallback(id => { dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id }) }, [dispatch])

  useEffect(() => {
    if (!actions) {
      setActions({ ...props.actions, ...Header.defaultProps.actions })
    }
  }, [
    props,
    actions
  ])

  const getCreateNew = () => {
    if (actions) {
      const { create: { enabled } } = actions
      if (enabled) {
        return (
          <Tooltip placement="top" title="Create new">
            <a
              tabIndex="0"
              className="btn btn-sm btn-light ml-2"
              role="button"
              onKeyDown={() => { }}
              onClick={() => {
                setRecord(null)
                setContentViewMode('new')
                toggleDrawer()
              }}
            >
              <i className="fe fe-plus" />
            </a>
          </Tooltip>
        )
      }
    }

    return null
  }

  const getReload = () => {
    if (actions) {
      const { reload: { enabled } } = actions
      const { listLoading } = props[STORE_PREFIX]
      if (enabled) {
        return (
          <Tooltip placement="top" title="Reload">
            <a
              tabIndex="0"
              className="btn btn-sm btn-light ml-2"
              role="button"
              onKeyDown={() => { }}
              onClick={getList}
            >
              <SyncOutlined spin={listLoading} />
            </a>
          </Tooltip>
        )
      }
    }

    return null
  }

  // const getFilters = () => {
  //   console.log(newActions)
  //   const { filters: { enabled, node } } = newActions

  //   if (enabled) {
  //     return node
  //   }

  //   return null
  // }


  return (
    <div className="card-header-flex align-items-center">
      <div className="d-flex flex-column justify-content-center mr-auto">
        <h5 className="mb-0">
          <strong>{data.title}</strong>
        </h5>
      </div>
      <div>

        {/* {getFilters()} */}


        {getCreateNew()}

        {getReload()}
      </div>
    </div>
  )
}

Header.defaultProps = {
  data: {
    title: 'With actions',
  },
  actions: {
    reload: {
      enabled: true,
    },
    create: {
      enabled: true
    },
    filters: {
      enabled: false,
      node: null
    }
  }
}

export default connect()(Header)
