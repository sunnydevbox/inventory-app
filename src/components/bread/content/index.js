import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Drawer } from 'antd'
import FooterButtonsComponent from 'components/bread/content/drawer-buttons'
import { STORE_PREFIX, DRAWER_WIDTH, DRAWER_PLACEMENT } from 'components/bread/config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
  }
}

const DrawerContentManagerComponent = ({
  formCancel,
  content,
  ...props
}) => {

  const [config, setConfig] = useState(null)

  useEffect(() => {
    if (!config) {
      setConfig(props.config)
    }
  }, [
    config,
    props
  ])

  const getDrawerWidth = () => {
    if (config) {
      return config.DRAWER_WIDTH
    }

    return DRAWER_WIDTH
  }

  const getDrawerPlacement = () => {
    if (config) {
      return config.DRAWER_PLACEMENT
    }

    return DRAWER_PLACEMENT
  }

  const getViewContent = () => {
    const { viewMode } = props[STORE_PREFIX]
    if (content && viewMode === 'view') {
      return content.view
    }

    return null
  }

  const getFormContent = () => {
    const { viewMode } = props[STORE_PREFIX]
    if (content && (viewMode === 'edit' || viewMode === 'new')) {
      return content.form
    }

    return null
  }

  return (
    <>
      <Drawer
        // title={getDrawerTitle()}
        placement={getDrawerPlacement()}
        height={getDrawerWidth()}
        closable={false}
        onClose={() => {
          formCancel()
        }}
        visible={props[STORE_PREFIX].drawerState}
        footer={<FooterButtonsComponent content={content} {...props} />}
      >
        {getViewContent()}
        {getFormContent()}
      </Drawer>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContentManagerComponent)
