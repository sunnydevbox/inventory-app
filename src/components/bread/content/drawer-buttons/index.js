import React, { useEffect, useCallback } from 'react'
import { Modal, Button } from 'antd'
import { connect } from 'react-redux'
import parser from 'html-react-parser'
import {
  ExclamationCircleOutlined, EyeOutlined,
  EditOutlined, SaveOutlined,
  DeleteOutlined, ExportOutlined,
} from '@ant-design/icons';
import { STORE_PREFIX } from 'components/bread/config'
import './styles.scss'

const { confirm } = Modal
const FooterButtonsComponent = ({
  dispatch,
  ...props
}) => {
  // const [modalConfirmDeleteState, setModalConfirmDeleteState] = useState(0)

  const setFormTouched = useCallback(status => dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, status }), [dispatch])
  // const formCancel = useCallback(force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }), [dispatch])
  const setContentViewMode = useCallback(viewMode => dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }), [dispatch])
  const commitRecord = useCallback(async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }), [dispatch])
  // const selectRecord = useCallback(async data => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, data }), [dispatch])
  const deleteRecord = useCallback(id => dispatch({ type: `${STORE_PREFIX}/API_DELETE_RECORD`, id }), [dispatch])
  const toggleDrawer = useCallback(() => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }), [dispatch])

  useEffect(() => {
    // console.log(props)
  }, [props])

  const getSaveButton = () => {
    const { viewMode, loadingSelectedRecord, formTouched } = props[STORE_PREFIX]

    let classFormTouched = ''
    if (formTouched) {
      classFormTouched = 'touched'
    }
    if (viewMode === 'edit' || viewMode === 'new') {
      return (
        <>
          <Button
            className={`btn btn-success ml-1 ${classFormTouched}`}
            role="button"
            tabIndex="0"
            onKeyUp={null}
            disabled={loadingSelectedRecord}
            onClick={() => {
              try {
                props[STORE_PREFIX].formReference
                  .validateFields()
                  .then(values => {
                    // props[STORE_PREFIX].formReference.resetFields()
                    // console.log(1, selectedRecord)
                    commitRecord(values)
                    // console.log(2,selectedRecord)
                    // selectRecord(selectedRecord.id)
                    setContentViewMode('edit')
                  })
                  .catch(info => {
                    console.log('Validate Failed:', info)
                  })
              } catch (error) {
                console.log(1, error)
              }
            }}
          >
            <SaveOutlined /> Save
          </Button>
        </>
      )
    }

    return null
  }

  const getSaveAndCloseButton = () => {
    const { viewMode, loadingSelectedRecord, formTouched } = props[STORE_PREFIX]

    let classFormTouched = ''
    if (formTouched) {
      classFormTouched = 'touched'
    }
    if (viewMode === 'edit' || viewMode === 'new') {
      return (
        <>
          <Button
            className={`btn btn-success ml-1 ${classFormTouched}`}
            role="button"
            tabIndex="0"
            onKeyUp={null}
            disabled={loadingSelectedRecord}
            onClick={() => {
              // console.log(STORE_PREFIX, props[STORE_PREFIX].formReference)
              props[STORE_PREFIX].formReference
                .validateFields()
                .then(values => {
                  // props[STORE_PREFIX].formReference.resetFields()
                  commitRecord(values)
                  toggleDrawer()
                })
                .catch(info => {
                  console.log('Validate Failed:', info)
                })
            }}
          >
            <SaveOutlined /> Save &amp; Close
          </Button>
        </>
      )
    }

    return null
  }

  const getCloseButton = () => {
    const { formTouched } = props[STORE_PREFIX]

    return (
      <>
        <a
          className="btn btn-warning ml-1"
          role="button"
          tabIndex="0"
          onKeyUp={null}
          onClick={() => {
            if (formTouched) {
              confirm({
                title: `You have made changes to this form`,
                icon: <ExclamationCircleOutlined />,
                content: parser('If you wish to continue, you will lose any changes made. <br />Continue?'),
                onOk() {
                  // const { formReference } = props[STORE_PREFIX]
                  // console.log(formReference )
                  // if (formReference) {
                  //   formReference.resetFields()
                  // }
                  setFormTouched(false)
                  toggleDrawer()
                },
                okText: 'Yes',
                cancelText: 'No',
              })
            }
            else {
              // props[STORE_PREFIX].formReference.resetFields()
              setFormTouched(false)
              toggleDrawer()
            }
          }}
        >
          <ExportOutlined /> Close
        </a>
      </>
    )
  }


  const getEditButton = () => {
    const { viewMode } = props[STORE_PREFIX]
    if (viewMode === 'view') {
      return (
        <>
          <a
            className="btn btn-primary ml-1"
            role="button"
            tabIndex="0"
            onKeyUp={null}
            onClick={(e) => {
              setContentViewMode('edit')
              e.preventDefault()
            }}
          >
            <EditOutlined /> Edit
          </a>
        </>
      )
    }

    return null
  }

  const getCancelButton = () => {
    const { viewMode, formTouched } = props[STORE_PREFIX]
    if (viewMode === 'edit') {
      return (
        <>
          <a
            className="btn btn-primary ml-1"
            role="button"
            tabIndex="0"
            onKeyUp={null}
            onClick={(e) => {
              if (formTouched) {
                confirm({
                  title: `You have made changes to this form`,
                  icon: <ExclamationCircleOutlined />,
                  content: parser('If you wish to continue, you will lose any changes made. <br />Continue?'),
                  onOk() {
                    setFormTouched(false)
                    setContentViewMode('view')
                  },
                  okText: 'Yes',
                  cancelText: 'No',
                })
              }
              else {
                setFormTouched(false)
                setContentViewMode('view')
              }
              // setContentViewMode('view')
              e.preventDefault()
            }}
          >
            <EyeOutlined /> Back To View Mode
          </a>
        </>
      )
    }

    return null
  }

  const getDeleteButton = () => {
    const { selectedRecord, viewMode } = props[STORE_PREFIX]

    if (viewMode === 'edit') {
      return (
        <>
          <a
            className="btn btn-danger btn-delete"
            role="button"
            tabIndex="0"
            onKeyUp={null}
            onClick={(e) => {
              confirm({
                title: `Do you want to delete this ${props.config.LABEL_SINGULAR}?`,
                icon: <ExclamationCircleOutlined />,
                content: 'This action cannot be reversed',
                onOk() {
                  deleteRecord(selectedRecord.id)

                },
                okText: 'Yes',
                cancelText: 'No',
              })

              // setModalConfirmDeleteState(true)
              // deleteRecord(selectedRecord.id)
              e.preventDefault()
            }}
          >
            <DeleteOutlined /> Delete
          </a>
        </>
      )
    }

    return null
  }

  const defaultButtons = () => {
    const { content } = props

    if (content && !content.overrideButtons) {
      return (
        <>
          <div className="text-center">
            {getEditButton()}
            {getSaveButton()}
            {getSaveAndCloseButton()}
            {getCancelButton()}
            {getCloseButton()}
            {getDeleteButton()}
          </div>
        </>
      )
    }

    return null
  }

  const overrideButtons = () => {
    const { content } = props

    if (content && content.overrideButtons) {
      return content.overrideButtons
    }

    return null
  }

  return (
    <>
      {defaultButtons()}
      {overrideButtons()}

      {/* 
        Edit
        Cancel - cancel edit
        Close - closes drawer
      */}

      {/* {drawerFooter()} */}
      {/* <Modal
        title="Confirm"
        visible={modalConfirmDeleteState}
        onOk={() => {
          // formCancel(true)

          // - CALL API
          // - close this modal
          // - cancelForm
          // - refresh table
          deleteRecord(selectedRecord.id)
          setModalConfirmDeleteState(false)
          formCancel({ force: true })
        }}
        onCancel={() => {
          setModalConfirmDeleteState(false)
        }}
        okText="Yes"
        cancelText="No"
      >
        <p>Are you sure you want to delete this record?</p>
      </Modal> */}
    </>
  )
}

export default connect()(FooterButtonsComponent)
