/* V1.0 */
export const LABEL_SINGULAR = 'Payment Term'
export const LABEL_PLURAL = 'Payment Terms'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featureBread'
export const LIST_LIMIT = 10