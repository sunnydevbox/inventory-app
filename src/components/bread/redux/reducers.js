import actions from './actions'
import { LIST_LIMIT } from '../config'

const initialState = {
  API_SERVICE: null,
  resetting: false,
  
  // DRAWER
  drawerState: false, // true or false for open or close

  // TABLE / LIST
  reloadListState: false,
  listLoading: true,
  dataList: [],
  pagination: {
    // Used for display
    total: 0,
    count: 0,
    current_page: 1,
    per_page: LIST_LIMIT,
    total_pages: 0,
  },

  // Filters`
  filters: {
    // orderBy: 'created_at',
    // sortedBy: 'desc',
    search: '',
    limit: LIST_LIMIT,
  },

  // Selected Event
  loadingSelectedRecord: false,
  selectedRecord: null,

  // Form-related
  formReference: null,
  viewMode: 'view', // new/edit/view/delete
  formTouched: false,
  formTriggerSave: false,
  formConfirmCancelModal: false,
}

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE:
      return { ...state, ...action.payload }
    case actions.RESET_STATE:
      // console.log('action.resetting', action.resetting)
      if (action.resetting) {
        return initialState
      }

      return state
    default:
      return state
  }
}
