/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useState, useEffect } from 'react'
import { Select, Spin } from 'antd'
import './styles.scss'

const { Option } = Select

const DropdownMultipleSelectComponent = ({ 
  setSelected, 
  Service, 
  // renderOptionItem, 
  callback,
  optionsData,
  // ...props 
}) => {
  // const [options, setOptions] = useState([])
  const [value, setValue] = useState([])
  const [fetching, setFetching] = useState(false)
  const [childrenOptions, setChildrenOPtions] = useState([])

  useEffect(() => {
    if (optionsData && optionsData.length) {
      setChildrenOPtions(
        optionsData.map(item => {
          const { id, name } = item
          return <Option key={id}>{name}</Option>
        })
      )
    }
  }, [
    optionsData,
  ])

  const handleSearch = str => {
    // setOptions([])
    setFetching(true)

    if (callback) {
      callback(value)
    }
    if (Service) {
      Service.dropdownResult(str)
        .then(response => {
          setFetching(false)
          setChildrenOPtions(
            response.data.map(item => {
              const { id, name } = item
              return <Option key={id}>{name}</Option>
            }),
          )
        })
        .catch(() => {
          setFetching(false)
        })
    }
  }

  const handleChange = str => {
    if (setSelected) {
      setSelected(str)
    }
    setValue(str)
  }

  return (
    <>
      <Select
        showSearch
        allowClear
        onClear={() => handleChange('')}
        value={value}
        defaultActiveFirstOption={false}
        showArrow={false}
        filterOption={false}
        onSearch={handleSearch}
        onChange={handleChange}
        notFoundContent={fetching ? <Spin size="small" /> : null}
        onFocus={() => {
          handleSearch('')
        }}
        // {...props}
      >
        {childrenOptions}
      </Select>
    </>
  )
}

export default DropdownMultipleSelectComponent
