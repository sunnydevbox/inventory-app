import React from 'react'
import { Input, Form } from 'antd'

const { Search } = Input
const FormItem = Form.Item

const SearchbarComponent = ({ callback, searchLoading, minChar, ...props }) => {
  // useEffect(() => {
  //   console.log('searchLoading', searchLoading)
  // }, [searchLoading])

  return (
    <>
      <Form
        onFinish={values => {
          callback(values.searchTerm)
        }}
      >
        <FormItem
          name="searchTerm"
          rules={[{ min: minChar, message: `Minimum of ${minChar} characters` }]}
        >
          <Search
            placeholder="Name"
            // onSearch={str => setSearchTerm(str)}
            style={{ width: 200 }}
            allowClear
            loading={searchLoading}
            disabled={searchLoading}
            {...props}
          />
        </FormItem>
      </Form>
    </>
  )
}

export default SearchbarComponent
