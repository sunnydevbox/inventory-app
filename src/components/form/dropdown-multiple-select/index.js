/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { 
  useState, 
  useEffect } from 'react'
import {
  Select,
  // Spin
} from 'antd'
import './styles.scss'

const { Option } = Select

const DropdownMultipleSelectComponent = ({ 
  options
  // setSelected, 
  // Service, 
  // renderOptionItem,  
}) => {
  // const [options, setOptions] = useState([])
  // const [value, setValue] = useState([])
  // const [fetching, setFetching] = useState(false)
  const [childrenOptions, setChildrenOPtions] = useState([])

  useEffect(() => {
    // console.log('prosp', props.children)

    // setChildrenOPtions(props.children)
    if (options) {
      console.log(options)
      setChildrenOPtions(options.forEach(row => {
        return <Option key={row.id} value={row.id}>{row.name}</Option>
      }))
    }
  }, [options])

  return (
    <>
      <Select 
        mode="multiple" 
      >
        {childrenOptions}
      </Select>
    </>
  )
}

export default DropdownMultipleSelectComponent
