import React, { useEffect,useState } from 'react'
import { Select } from 'antd'
import { connect } from 'react-redux'
import './styles.scss'

const mapStateToProps = state => ({ settings: state.settings })
const mapDispatchToProps = null

const SettingsDropdownSimpleComponent = ({
  callback,
  selected,
  type,
  styles,
  withAll,
  // placeholder,
  mode,
  // allowClear
  settings: { suppData },
  // ...props
}) => {
  const [options, setOptions] =  useState([])

  useEffect(() => {
    if (suppData && suppData[type]) {
      if (withAll) {
        suppData[type].push({ id: 'all', name: 'All' })
      }

      const opts = suppData[type].map(row => {
        return { value: row.id, label: row.name }
      })
      setOptions(opts)
    }
  }, [type, suppData, withAll, mode])

  // const handleSave = async values => {
  //   console.log('handleSave')
  //   commitRecord(values)
  // }

  return (
    <Select
      mode={mode}
      options={options}
      defaultValue={selected}
      value={selected}
      onChange={v => {
        callback(v)
      }}
      {...styles}
      // {...props}
    />
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsDropdownSimpleComponent)
