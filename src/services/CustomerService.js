import API from './AbstractAPI'

class CustomerService extends API {
  endpoint = '/customers'

  dropdownResult = search => {
    return this.axiosAPI
      .get(`${this.apiURL()}/dropdown${this.buildQueryString({ search })}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }
}

const service = new CustomerService()
export default service
