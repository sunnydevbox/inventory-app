import API from './AbstractAPI'

export default class POSService extends API {
  endpoint = '/pos'

  getStoreCategories = storeId => {
    this.apiURL(`/pos/store/${storeId}/categories`)
    return this.index()
      .then(res => {
        return res
      })
      .catch(err => console.log(err))
  }

  getStoreStocks = storeId => {
    this.apiURL(`/pos/store/${storeId}/stocks`)
    return this.index()
      .then(res => {
        return res
      })
      .catch(err => {
        return Promise.reject(err)
      })
  }

  getCurrentTransaction = (storeId, posId) => {
    this.apiURL(`/pos/sale/current`)
    return this.post({
      store_id: storeId,
      pos_id: posId,
    })
      .then(res => {
        return res.data
      })
      .catch(err => console.log(err))
  }

  addItemToCart = item => {
    this.apiURL(`/pos/sale/item/add`)
    return this.post(item)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err)
      })
  }

  removeItemFromCart = saleItemId => {
    this.apiURL(`/pos/sale/item/remove`)
    return this.post({
      sale_item_id: saleItemId,
    })
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err)
      })
  }

  discardTransaction = transactionId => {
    this.apiURL(`/pos/sale`)
    return this.destroy(transactionId)
      .then(res => {
        return res
      })
      .catch(err => {
        return Promise.reject(err)
      })
  }

  newTransaction = data => {
    this.apiURL(`/pos/sale`)
    return this.post(data)

      .then(res => {
        return res
      })
      .catch(err => {
        return Promise.reject(err)
      })
  }

  process = data => {
    this.apiURL(`/pos/sale/process`)
    return this.post(data)
      .then(res => {
        return res
      })
      .catch(err => {
        return Promise.reject(err)
      })
  }
}

const service = new POSService()

export async function getStoreCategories(storeId) {
  return service.getStoreCategories(storeId)
}

export async function getStoreStocks(storeId) {
  return service.getStoreStocks(storeId)
}

export async function getCurrentTransaction(storeId, posId) {
  return service.getCurrentTransaction(storeId, posId)
}

export async function addItemToCart(item) {
  return service.addItemToCart(item)
}

export async function removeItemFromCart(saleItemId) {
  return service.removeItemFromCart(saleItemId)
}

export async function discardTransaction(saleItemId) {
  return service.discardTransaction(saleItemId)
}

export async function newTransaction(data) {
  return service.newTransaction(data)
}

export async function process(data) {
  return service.process(data)
}
