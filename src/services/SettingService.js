import API from './AbstractAPI'

// const Service = new API;
// Service.apiURL('/courses');

class SettingService extends API {
  endpoint = '/settings'

  details = () => {
    return this.axiosAPI
      .get(`${this.apiURL()}/pub/details`)
      .then(res => {
        return res
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  getCompany = (subdomain) => {
    return this.axiosAPI
      .post(`${this.apiURL()}/c/details`, { subdomain })
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  suppdata = (params = null) => {
    params = params ? `data=${params}` : ``
    return this.axiosAPI
      .get(`${this.apiURL()}/suppdata?${params}`)
      .then(res => {
        return res
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }
}

const service = new SettingService()
export default service
