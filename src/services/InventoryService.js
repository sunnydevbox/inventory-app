/* eslint-disable camelcase */
import API from './AbstractAPI'

export default class InventoryService extends API {
  endpoint = '/inventory'

  search(params) {
    return this.axiosAPI
      .get(`${this.apiURL()}/search${this.buildQueryString(params)}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }

  addConversion(data) {
    return this.axiosAPI
      .post(`${this.apiURL()}/unit-conversion`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  updateConversion(conversion_id, data) {
    return this.axiosAPI
      .put(`${this.apiURL()}/unit-conversion/${conversion_id}`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  removeConversion(conversion_id) {
    return this.axiosAPI
      .delete(`${this.apiURL()}/unit-conversion/${conversion_id}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }

  simpleList() {
    return this.index({
      filter: 'id;name;code',
      orderBy: 'name',
      sortedBy: 'asc',
      limit: 0,
    })
      .then(res => {
        return res.data
      })
      .catch(err => {
        return err
        // console.log(err)
      })
  }
}

export async function getSimpleList() {
  const service = new InventoryService()
  return service.simpleList()
}
