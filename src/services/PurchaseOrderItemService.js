import API from './AbstractAPI'

class PurchaseOrderItemService extends API {
  endpoint = '/purchase-order-items'

  store = (poId, data = {}) => {
    this.endpoint = `${this.endpoint}/${poId}`
    return this.axiosAPI
      .post(this.apiURL(), data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }
}
const service = new PurchaseOrderItemService()
export default service
