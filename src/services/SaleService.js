import API from './AbstractAPI'

// const Service = new API;
// Service.apiURL('/courses');

export default class SaleService extends API {
  endpoint = '/sales'
}

// const service = new SaleService()
