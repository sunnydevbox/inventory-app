import API from './AbstractAPI'

export default class ProfileService extends API {
  endpoint = '/profile'

  updateEmail(uuid, data) {
    return this.axiosAPI
      .post(`${this.apiURL()}/update-email/${uuid}`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err);
        return Promise.reject(err)
      })
  }

  updatePassword(uuid, data) {
    return this.axiosAPI
      .post(`${this.apiURL()}/update-password/${uuid}`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err);
        return Promise.reject(err)
      })
  }
}
