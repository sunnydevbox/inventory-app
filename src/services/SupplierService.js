import API from './AbstractAPI'

class SupplierService extends API {
  endpoint = '/suppliers'

  getPaymentTerms = (supplierId) => {
    return this.axiosAPI
      .get(`${this.apiURL()}/${supplierId}/payment-terms`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

}

const service = new SupplierService()
export default service
