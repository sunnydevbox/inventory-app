import API from './AbstractAPI'

export default class StockService extends API {
  endpoint = '/stocks'

  checkIn(data) {
    return this.axiosAPI
      .post(`${this.apiURL()}/check-in`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }

  checkOut(data) {
    return this.axiosAPI
      .post(`${this.apiURL()}/check-out`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }

  getStockMovement(stockId, params) {
    return this.axiosAPI
      .get(`${this.apiURL()}/movement/${stockId}${this.buildQueryString(params)}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }

  search(params) {
    return this.axiosAPI
      .get(`${this.apiURL()}/search${this.buildQueryString(params)}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }

  simpleList() {
    return this.index({
      filter: 'uuid;name;code',
      orderBy: 'name',
      sortedBy: 'asc',
      limit: 0,
    })
      .then(res => {
        return res.data
      })
      .catch(err => {
        return err
        // console.log(err)
      })
  }

  getUnitsByStock(stockId) {
    return this.axiosAPI
      .get(`${this.apiURL()}/${stockId}/units`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }
}

export async function getSimpleList() {
  const service = new StockService()
  return service.simpleList()
}

export async function checkIn(data) {
  const service = new StockService()
  return service.checkIn(data)
}

export async function getStockMovement(stockId, data) {
  const service = new StockService()
  return service.getStockMovement(stockId, data)
}

export async function getUnitsByStock(stockId) {
  const service = new StockService()
  return service.getUnitsByStock(stockId)
}
