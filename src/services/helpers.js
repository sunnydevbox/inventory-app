/* eslint-disable object-shorthand */
/* eslint-disable react/no-deprecated */
/* eslint-disable prefer-template */
import React from 'react'
import { notification } from 'antd'
import parser from 'html-react-parser'
import moment from 'moment'

export default function badge(label) {
  let type = 'success'
  switch (label.toLowerCase()) {
    case 'pending':
      type = 'warning'
      break

    case 'dropped':
    case 'suspended':
      type = 'danger'
      break

    default:
    case 'enrolled':
      type = 'success'
      break
  }

  return type
}

export function ucwords(str) {
  // const words = str.split(/\s+/);

  return str.replace(/\w\S*/g, txt => {
    return txt.substring(0, 1).toUpperCase() + txt.substr(1).toLowerCase()
  })

  // words.map((word) => {
  //     console.log(word.substring(0,1).toUpperCase() + word.slice(1));
  //     return word
  // })
  // str.charAt(0).toUpperCase() + this.slice(1);
  // return [ first.toLocaleUpperCase(locale), ...rest ].join('');
}

export function parse(messages) {
  if (typeof messages === 'object') {
    if (messages.data && messages.data.message) {
      const { message: m1 } = messages.data
      messages = m1
    }
  }

  if (typeof messages === 'string') {
    return messages
  }

  const text = Object.keys(messages).reduce((res, v) => {
    return res.concat('<br/>' + messages[v])
  }, [])

  // const br = React.createElement('br');
  // const regex = /(<br \/>)/g;
  // text = text.split(regex).map((line, index) => {
  //     return line.match(regex) ? <br key={`key ${index}`} /> : line;
  // });

  // const Text = React.createClass({
  //   render: () => {
  //       return <div>{ text }</div>;
  //   }
  // });

  // return React.render(<Text content="Here are some <br /> Lines <br /> to break!" />, document.body);

  return React.createElement('div', {}, text)
  // return React.createElement('h1', {}, arr)
}

export function translateStatus(s) {
  let status = ''

  switch (s) {
    case 'a':
      status = 'active'
      break

    case 'i':
      status = 'inactive'
      break

    case 'ar':
      status = 'archived'
      break

    default:
      status = null
      break
  }

  return status
}

export function translateSaleStatus(s) {
  let status = ''

  switch (s) {
    case 'o':
      status = 'open'
      break

    case 'h':
      status = 'hold'
      break

    case 'p':
      status = 'processing'
      break

    case 'c':
      status = 'complete'
      break

    case 'cl':
      status = 'closed'
      break

    default:
      status = null
      break
  }

  return status
}

export function Notification(serverData) {
  let type = 'info'
  let title = 'Info'
  let description = ''

  if (serverData) {
    if (serverData.status) {
      if (serverData.status >= 400 && serverData.status <= 499) {
        type = 'warning'
        title = 'Warning'
      } else if (serverData.status >= 200 && serverData.status <= 299) {
        type = 'success'
        title = 'Success'
      } else if (serverData.status >= 500 && serverData.status <= 599) {
        type = 'error'
        title = 'Error'
      }
    }

    if (serverData.description) {
      ;({ description } = serverData)
    } else if (serverData.data && serverData.data.message) {
      if (typeof serverData.data.message === 'object') {
        Object.keys(serverData.data.message).forEach(field => {
          // description += `<strong>${field}</strong>`

          if (typeof serverData.data.message[field] === 'object') {
            Object.values(serverData.data.message[field]).forEach(err => {
              description += `${err}<br />`
            })
          }
        })
      } else {
        console.log('here')
        description = serverData.data.message
      }
    }

    // Title
    if (serverData && serverData.title) {
      ;({ title } = serverData)
    }
  } else {
    type = 'error'
    description='There was a minor hiccup. We have notified the administrator.'
  }

  notification[type]({
    message: title,
    description: parser(description),
  })
}

export function getSubdomain() {
  const parts = window.location.hostname.split('.')
  const subdomain = parts.shift()

  return subdomain.toLowerCase()
}

export function renderField(label, value) {
  return (
    <div className="field mt-4">
      {value && value.length ? (
        <div className="pl-1" style={{ whiteSpace: 'pre' }}>
          {value}
        </div>
      ) : (
        <em className="pl-1 text-muted">Not specified</em>
      )}
      <hr className="m-0" />
      <span className="pl-1 text-muted">{label}</span>
    </div>
  )
}

export function formatDate(date, format = 'LL') {
  return moment(date).format(format)
}

export function actionPermission(permission, userPermissions) {
  console.log(permission, userPermissions)
}


export function poStateTranslation(state) {
  switch(state) {
    /*
    STATE_PO_UNAPPROVED       = 'unapproved';
    const STATE_PO_PARKED           = 'parked';
    const STATE_PO_PLACED           = 'placed';
    const STATE_PO_COSTED           = 'costed';
    const STATE_PO_RECEIPTED        = 'receipted';
    const STATE_PO_PARTIAL_SERVED   = 'partial_served';
    const STATE_PO_COMPLETED        = 'complete';
    const STATE_PO_DELETED          = 'deleted';
    */
    case 'unapproved':
      return 'disapproved'
    case 'placed':
      return 'placed'
    case 'parked':
      return 'parked'
    case 'costed':
      return 'costed'
    case 'costing':
      return 'costing'
    case 'complete':
      return 'complete'
    case 'partial_served':
      return 'partially Served'
    case 'deleted':
      return 'deleted'
    default:
      return state
  }
}