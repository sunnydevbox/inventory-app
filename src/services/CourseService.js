import API from './AbstractAPI'

// const Service = new API;
// Service.apiURL('/courses');

export default class CourseService extends API {
  endpoint = '/courses'

  simpleList() {
    return this.index({
      filter: 'uuid;name;code',
      orderBy: 'name',
      sortedBy: 'asc',
      limit: 0,
    })
      .then(res => {
        return res.data
      })
      .catch(err => {
        return err
      })
  }
}

export async function getSimpleList() {
  const service = new CourseService()
  return service.simpleList()
}
