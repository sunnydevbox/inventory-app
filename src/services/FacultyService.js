import API from './AbstractAPI'

// const Service = new API;
// Service.apiURL('/courses');

export default class FacultyService extends API {
  endpoint = '/faculty'

  // uploadAvatar(id) {

  //     {{url}}/faculty/upload-avatar/cebfc5d0-8c39-11ea-8a32-b3df59a53cde
  // }

  uploadFile(file, id) {
    return this.axiosAPI
      .post(`${this.apiURL()}/upload-avatar/${id}`, file, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then(res => {
        return res
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }
}
