import API from './AbstractAPI'

class MetricService extends API {
  endpoint = '/metrics'

  dropdownResult = search => {
    return this.axiosAPI
      .get(`${this.apiURL()}/dropdown${this.buildQueryString({ search })}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }
}

const service = new MetricService()
export default service
