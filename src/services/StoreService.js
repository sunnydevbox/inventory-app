import API from './AbstractAPI'

// const Service = new API;
// Service.apiURL('/courses');

export default class StoreService extends API {
  endpoint = '/stores'

  attachWarehouse = (storeId, warehouseId) => {
    return this.axiosAPI
      .post(`${this.apiURL()}/${storeId}/warehouse/${warehouseId}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }

  detachWarehouse = (storeId, warehouseId) => {
    return this.axiosAPI
      .delete(`${this.apiURL()}/${storeId}/warehouse/${warehouseId}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        // console.log('What happened? ' + err.response.data);
        return Promise.reject(err.response)
      })
  }
}

const service = new StoreService()

export async function attachWarehouse(storeId, warehouseId) {
  return service.attachWarehouse(storeId, warehouseId)
}

export async function detachWarehouse(storeId, warehouseId) {
  return service.detachWarehouse(storeId, warehouseId)
}
