import API from './AbstractAPI'

class LocationService extends API {
  endpoint = '/locations'
}

const service = new LocationService()
export default service
