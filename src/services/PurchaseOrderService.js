import API from './AbstractAPI'

class PurchaseOrderService extends API {
  endpoint = '/purchase-orders'

  addItem = (poId, data = {}) => {
    data.po_id = poId

    return this.axiosAPI
      .post(`${this.apiURL()}/items`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  editItem = (id, data = {}) => {
    return this.axiosAPI
      .put(`${this.apiURL()}/items/${id}`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  setState = (poId, state) => {
    return this.axiosAPI
      .post(`${this.apiURL()}/${poId}/set-state`, {
        state
      })
      .then(res => {
        return res.data
        // return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  serve = (poItemId, data = {}) => {
    return this.axiosAPI
      .post(`${this.apiURL()}/items/${poItemId}/serve`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }
}
const service = new PurchaseOrderService()
export default service
