import API from './AbstractAPI'

// const Service = new API;
// Service.apiURL('/courses');

export default class SubjectService extends API {
  endpoint = '/subjects'

  simpleList() {
    return this.index({
      filter: 'uuid;name;code',
      orderBy: 'name',
      sortedBy: 'asc',
      limit: 0,
    })
      .then(res => {
        return res.data
      })
      .catch(err => {
        return err
        // console.log(err)
      })
  }
}

export async function getSimpleList() {
  const service = new SubjectService()
  return service.simpleList()
}
