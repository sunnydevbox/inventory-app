import API from './AbstractAPI'

class CategoryService extends API {
  endpoint = '/categories'

  dropdownResult = search => {
    return this.axiosAPI
      .get(`${this.apiURL()}/dropdown${this.buildQueryString({ search })}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }
}

const service = new CategoryService()
export default service
