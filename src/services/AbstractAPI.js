/* eslint-disable prefer-template */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-lonely-if */

import axios from 'axios'
import store from 'store'
import { getSubdomain, Notification } from 'services/helpers'
import { notification } from 'antd'

// import store from 'react-native-simple-store';
// import Config from 'react-native-config';
// import { showMessage, hideMessage } from "react-native-flash-message";
// import { NavigationActions, NavigationState } from 'react-navigation';

// console.log(__DEV__, 'Config',Config, URL)

const subdomain = getSubdomain()
// const upperleveldomain = parts.join('.');
// define the api

const axiosAPI = axios.create({
  // baseURL: `http://${subdomain}.${process.env.REACT_APP_DOMAIN}`, // process.env.REACT_APP_API_URL,
  baseURL: `http://${process.env.REACT_APP_DOMAIN}`, // process.env.REACT_APP_API_URL,
  timeout: 10000,
  headers: {
    Accept: process.env.REACT_APP_API_HEADER_ACCEPT,
    'Content-Type': process.env.REACT_APP_API_HEADER_CONTENT_TYPE,
    // Tenant: `${subdomain}.${process.env.REACT_APP_DOMAIN}`,
    Tenant: `${subdomain}`,
  },
})

/*
 * INTERCEPTORS
 */
axiosAPI.interceptors.request.use(
  async config => {
    const token = await store.get('token')

    if (token) {
      config.headers.authorization = `Bearer ${token}`
    }

    // config.headers.authorization = 'Bearer ' + await AsyncStorage.getItem('userToken');
    // console.log(config);
    return config
  },
  error => {
    console.log(error)

    // Do something with request error
    return Promise.reject(error)
  },
)

axiosAPI.interceptors.response.use(
  response => {
    // console.log('axiosAPI.interceptors.response', response)
    // Do something with response data
    return response
  },
  error => {
    // let message = 'There was problem with the system. Please try again later.'

    // console.log('**** ERRIOR', error, error.response.status)

    if (!error.response) {
      // notification.error({
      //   message: 'Oops...',
      //   description: 'We are experiencing some difficulties.',
      //   duration: 10,
      // })
    } else {
      switch (error.response.status) {
        case 400:
          Notification(error.response)
          break

        case 401:
          
          if (error.response.data.error === 'invalid_credentials') {
            notification.warning({
              message: 'Invalid email or password',
              description: 'Email or password is invalid!',
            })
          } else {
            // if(error.response.data.message === 'Unauthenticated') {
            if (window.location.pathname !== '/auth/login') {
              notification.warning({
                message: 'Your session has expired. Please log in again.',
                // description: 'Email or password is invalid!',
              })
            }
            //   if (window.location.pathname !== '/auth/login') {
            //       // console.log('redirecting', store.clearAll(), window.location)
            //       // CLEAR SAGA/LOGOUT
            //       window.location.href = '/auth/login'
            //     }
            // }
          
          }
          break

        case 403:
          notification.warning({
            message: 'You are not authorized to perform this action',
            // description: 'Email or password is invalid!',
          })
          break

        case 404:
          notification.warning({
            message: 'We are having problem communicating with system',
            // description: 'Email or password is invalid!',
          })
          break

        case 500:
          notification.danger({
            message: 'The server is encountered a problem',
            // description: 'Email or password is invalid!',
          })
          break

        default:
          break
      }

    }

    return Promise.reject(error)
  },
)

class API {
  endpoint = null

  instance = axiosAPI

  sq = ''

  apiURL(endpoint = null) {
    if (endpoint) {
      this.endpoint = endpoint
    }

    return endpoint || this.endpoint
    // return endpoint ? endpoint : this.endpoint;
  }

  index = (params = {}) => {
    return this.axiosAPI
      .get(
        `${this.apiURL()}${this.buildQueryString(params)}`, 
      )
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  show = (id, params) => {
    // const source = this.axiosAPI.CancelToken.source(); 
    return this.axiosAPI
      .get(
        `${this.apiURL()}/${id}${this.buildQueryString(params)}`
      )
      .then(res => {
        return res.data
      })
      .catch(err => {
        console.log(err)

        if (this.axiosAPI.isCancel(err)) {
          return null;
        }

        return Promise.reject(err.response)
      })
  }

  update = (id, data = {}) => {
    return this.axiosAPI
      .put(`${this.apiURL()}/${id}`, data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        console.log('What happened? ' + err.response.data)
        return Promise.reject(err.response)
      })
  }

  destroy = (id, data = {}) => {
    return this.axiosAPI
      .delete(`${this.apiURL()}/${id}`, { data })
      .then(res => {
        return res
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  store = (data = {}) => {
    return this.axiosAPI
      .post(this.apiURL(), data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  post = (data = {}) => {
    return this.axiosAPI
      .post(this.apiURL(), data)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err)
      })
  }

  setHeader(key, value) {
    this.axiosAPI.defaults.headers[key] = value
  }

  buildQueryString(filterQuery) {
    this.sq = ''
    if (filterQuery) {
      this.sq += '?'
    }

    for (const filter in filterQuery) {
      if (typeof filter === 'string') {
        this.sq += `${filter}=${filterQuery[filter]}&`
      } else if (typeof filter === 'object') {
        // return new Promise(() => {
        //   this.sq = Object.keys(filter).map(
        //     (key) => {
        //         sq += `${key}=${filter[key]}`;
        //         return sq;
        //     }
        //   );
        // })
      }
    }
    return this.sq
  }

  constructor() {
    this.axiosAPI = axiosAPI
  }
}

export default API
