// let { API } = require('./AbstractAPI');
import store from 'store'
import API from './AbstractAPI'

const UserService = new API()
UserService.apiURL('/users')

export async function login(email, password, type) {
  UserService.apiURL('/login')

  return UserService.axiosAPI
        .post(UserService.apiURL(), { email, password, type })
        .then(res => {
          return res.data
        })
        .catch(err => {
          return err.response
        })
}

export async function me() {
  UserService.apiURL('/me')

  const response = await UserService.post()
    .then(res => {
      return res
    })
    .catch(err => {
      return err.response
    })

  return response
}

export async function currentAccount() {
  return store.get('loggedInUser')
}

export async function setToken(token) {
  store.set('token', token)
}

export async function setAccount(user) {
  console.log('setaccount', user)
  store.remove('loggedInUser')
  store.set('loggedInUser', user)
}

export async function logout() {
  store.remove('loggedInUserAuth')
  store.remove('token')
  store.remove('loggedInUser')
  return true
}

export default UserService
