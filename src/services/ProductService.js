import API from './AbstractAPI'

class ProductService extends API {
  endpoint = '/products'

  // dropdownResult = search => {
  //   // console.log(search)
  //   // return this.axiosAPI
  //   //   .get(`${this.apiURL()}/dropdown${this.buildQueryString({ search })}`)
  //   //   .then(res => {
  //   //     return res.data
  //   //   })
  //   //   .catch(err => {
  //   //     return Promise.reject(err.response)
  //   //   })
  // }

  skuUpdate = (inventoryId, code) => {
    return this.axiosAPI
      .post(`${this.apiURL()}/${inventoryId}/sku`, {
        code,
      })
      .then(res => {
        return res
        // return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  graphStockLastMovement = inventoryId => {
    return this.axiosAPI
      .post(`${this.apiURL()}/${inventoryId}/graph/last-movement`)
      .then(res => {
        return res
        // return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  imageUpload = (inventoryId, file) => {
    const endpoint = `${this.apiURL()}/${inventoryId}/image`
    console.log(file)
    const formData = new FormData()
    // const _headers = new HttpHeaders();
    formData.append('img', file)

    return this.axiosAPI
      .post(endpoint, formData, {
        reportProgress: true,
        responseType: 'json',
        // headers: _headers.append('enctype', 'multipart/form-data')
        //     .append('contentType', 'application/x-www-form-urlencoded')
      })
      .then(res => {
        return res
        // return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })

    // return this.axiosAPI
    //   .post(`${this.apiURL()}/${inventoryId}/image`, {img:file})
    //   .then(res => {
    //     return res
    //     // return res.data
    //   })
    //   .catch(err => {
    //     return Promise.reject(err.response)
    //   })
  }

  graphItemStockperLocation = inventoryId => {
    return this.axiosAPI
      .post(`${this.apiURL()}/${inventoryId}/graph/stock-per-location`)
      .then(res => {
        return res.data
        // return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  quickLatestStockMovement = inventoryId => {
    return this.axiosAPI
      .post(`${this.apiURL()}/${inventoryId}/graph/latest-stock-movement`)
      .then(res => {
        return res.data
        // return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  dropdownResult = (search, supplierId) => {
    return this.axiosAPI
      .get(`${this.apiURL()}/dropdown-search-stock?search=${search}&supplier=${supplierId}`)
      .then(res => {
        return res.data
        // return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  addSupplier = (inventoryId, supplierId, price = 0) => {
    return this.axiosAPI
      .post(`${this.apiURL()}/${inventoryId}/suppliers`, {
        supplier_id: supplierId,
        last_purchase_price: price,
      })
      .then(res => {
        return res.data
        // return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }


  editSupplier = (inventoryId, supplierId, price = 0) => {
    return this.axiosAPI
      .put(`${this.apiURL()}/${inventoryId}/suppliers/${supplierId}`, {
        last_purchase_price: price,
      })
      .then(res => {
        return res.data
        // return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  browseSuppliers = (inventoryId) => {
    return this.axiosAPI
      .get(`${this.apiURL()}/${inventoryId}/suppliers`)
      .then(res => {
        return res.data
        // return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  removeSupplier = (inventoryId, supplierId) => {
    return this.axiosAPI
      .delete(`${this.apiURL()}/${inventoryId}/suppliers/${supplierId}`)
      .then(res => {
        return res.data
        // return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

}

const service = new ProductService()
export default service
