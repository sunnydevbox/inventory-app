import API from './AbstractAPI'

class PaymentTermService extends API {
  endpoint = '/payment-terms'

  dropdownResult = search => {
    return this.axiosAPI
      .get(`${this.apiURL()}/dropdown${this.buildQueryString({ search })}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }
}

const service = new PaymentTermService()
export default service
