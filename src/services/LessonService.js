import API from './AbstractAPI'

export default class LessonService extends API {
  endpoint = '/lessons'

  loadVideo(key) {
    return this.axiosAPI
      .get(`${this.apiURL()}/video/${key}`)
      .then(res => {
        console.log('loadVideo(key) {', res)
        return res
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  // uploadVideo() {

  // }

  getClass(lessonCode) {
    return this.axiosAPI
      .get(`${this.apiURL()}/video-by-code/${lessonCode}`)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return Promise.reject(err.response)
      })
  }

  simpleList() {
    return this.index({
      filter: 'uuid;name;code',
      orderBy: 'name',
      sortedBy: 'asc',
      limit: 0,
    })
      .then(res => {
        return res.data
      })
      .catch(err => {
        return err
        // console.log(err)
      })
  }
}

export async function getSimpleList() {
  const service = new LessonService()
  return service.simpleList()
}
