import API from './AbstractAPI'

class CourierService extends API {
  endpoint = '/couriers'

  dropdownResult = (search) => {
    return this.axiosAPI
      .get(`${this.apiURL()}/dropdown${this.buildQueryString({ search })}`)
      .then((res) => {
        return res.data
      })
      .catch((err) => {
        return Promise.reject(err.response)
      })
  }
}

const service = new CourierService()
export default service
