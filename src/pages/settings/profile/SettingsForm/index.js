import React from 'react'
import { Form, Input, Button } from 'antd'
import { connect } from 'react-redux'

const FormItem = Form.Item

@connect(({ user }) => ({ user }))
// @Form.useForm()
class SettingsForm extends React.Component {
  state = {}

  render() {
    const { user } = this.props

    return (
      <div>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <h5 className="text-black mt-4">
            <strong>Personal Information</strong>
          </h5>
          <div className="row">
            <div className="col-lg-6">
              <Form.Item label="Username" name="userName" rules={[{ required: false }]}>
                <Input placeholder="Username" />
              </Form.Item>
            </div>
            <div className="col-lg-6">
              <FormItem label="Email" name="email" rules={[{ required: true }]}>
                <Input placeholder="Email" value={user.email} />
              </FormItem>
            </div>
          </div>
          <h5 className="text-black mt-4">
            <strong>New Password</strong>
          </h5>
          <div className="row">
            <div className="col-lg-6">
              <FormItem label="Password" name="password">
                <Input placeholder="New password" />
              </FormItem>
            </div>
            <div className="col-lg-6">
              <FormItem label="Confirm Password" name="confirmPassword">
                <Input placeholder="Confirm password" />
              </FormItem>
            </div>
          </div>
          <div className="form-actions">
            <Button style={{ width: 150 }} type="primary" htmlType="submit" className="mr-3">
              Submit
            </Button>
            <Button htmlType="submit">Cancel</Button>
          </div>
        </Form>
      </div>
    )
  }
}

export default SettingsForm
