import React, { Component } from 'react'
import { Input, Form, Button, DatePicker, Select, notification, Space, Spin } from 'antd'
import ProfileService from 'services/ProfileService'
import moment from 'moment/moment'

const { Option } = Select
const dateFormat = 'MMM DD, YYYY'

export default class PersonalForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      processing: false,
    }

    this.service = new ProfileService()
  }

  onGenderChange = value => {
    console.log(value)
  }

  handleSubmit = values => {
    const {
      user: { uuid },
    } = this.props

    if (values.dob) {
      values.dob = values.dob.format('YYYY-MM-DD')
    }

    this.setState({ processing: true })
    this.service
      .update(uuid, values)
      .then(() => {
        this.setState({ processing: false })
        notification.success({
          message: 'Personal Information Updated',
          // description: 'Email or password is invalid!',
        })
      })
      .catch(() => {
        this.setState({ processing: false })
      })
  }

  render() {
    const { user } = this.props
    const { processing } = this.state

    if (user.loading) {
      return (
        <div style={{ textAlign: 'center' }}>
          <Space size="middle">
            <Spin size="large" />
          </Space>
        </div>
      )
    }

    return (
      <div>
        <Form
          onFinish={this.handleSubmit}
          className="personal-information-form"
          layout="vertical"
          initialValues={{
            first_name: user.firstName,
            last_name: user.lastName,
            middle_name: user.middleName,
            dob: moment(user.dob),
            gender: user.gender,
            birth_place: user.birthPlace,

            address_1: user.address1,
            address_2: user.address2,
            city: user.city,
            state: user.state,
            country: user.country,
            zipcode: user.zipcode,
          }}
        >
          <div className="row">
            <div className="col-lg-4">
              <Form.Item
                name="first_name"
                label="First Name"
                rules={[{ required: true, message: ' ' }]}
              >
                <Input name="first_name" />
              </Form.Item>
            </div>
            <div className="col-lg-4">
              <Form.Item name="middle_name" label="Middle Name">
                <Input />
              </Form.Item>
            </div>
            <div className="col-lg-4">
              <Form.Item
                name="last_name"
                label="Last Name"
                rules={[{ required: true, message: ' ' }]}
              >
                <Input />
              </Form.Item>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-4">
              <Form.Item name="dob" label="Birth Date" rules={[{ required: true, message: ' ' }]}>
                <DatePicker
                  defaultPickerValue={moment('2015/01/01', dateFormat)}
                  format={dateFormat}
                />
              </Form.Item>
            </div>
            <div className="col-lg-4">
              <Form.Item
                name="birth_place"
                label="Birth Place"
                rules={[{ required: true, message: ' ' }]}
              >
                <Input />
              </Form.Item>
            </div>
            <div className="col-lg-4">
              <Form.Item name="gender" label="Gender" rules={[{ required: true }]}>
                <Select
                  initialvalues={[user.gender]}
                  value={user.gender}
                  onChange={this.onGenderChange}
                >
                  <Option value="1">Male</Option>
                  <Option value="0">Female</Option>
                </Select>
              </Form.Item>
            </div>
          </div>

          <h5 className="mb-3 text-black">
            <strong>Address</strong>
          </h5>

          <div className="row">
            <div className="col-lg-4">
              <Form.Item name="address_1" label="Address 1" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </div>

            <div className="col-lg-4">
              <Form.Item name="address_2" label="Address 2">
                <Input />
              </Form.Item>
            </div>

            <div className="col-lg-4">
              <Form.Item name="city" label="City" rules={[{ required: true, message: ' ' }]}>
                <Input />
              </Form.Item>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-4">
              <Form.Item
                name="state"
                label="Province/State"
                rules={[{ required: true, message: ' ' }]}
              >
                <Input />
              </Form.Item>
            </div>

            <div className="col-lg-4">
              <Form.Item name="country" label="Country" rules={[{ required: true, message: ' ' }]}>
                <Input />
              </Form.Item>
            </div>

            <div className="col-lg-4">
              <Form.Item name="zipcode" label="Zip Code" rules={[{ required: true, message: ' ' }]}>
                <Input />
              </Form.Item>
            </div>
          </div>

          <hr />
          <Button
            style={{ width: 150 }}
            type="primary"
            htmlType="submit"
            className="mr-3"
            loading={processing}
          >
            Save
          </Button>
          {/* <Button htmlType="submit">Cancel</Button> */}
        </Form>
      </div>
    )
  }
}
