import React, { Component } from 'react'
import { Input, Form, Button, Space, notification, Spin } from 'antd'
import { connect } from 'react-redux'
import ProfileService from 'services/ProfileService'

@connect(({ user }) => ({ user }))
export default class AccountForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      emailLoading: false,
      passwordLoading: false,
    }

    this.service = new ProfileService()
  }

  handleSubmitEmail = values => {
    const {
      user: { uuid },
      dispatch,
    } = this.props

    this.setState({ emailLoading: true })
    this.service
      .updateEmail(uuid, values)
      .then(() => {
        this.setState({ emailLoading: false })

        // UPDATE STORE
        dispatch({
          type: 'user/SET_STATE',
          payload: {
            email: values.email,
          },
        })

        notification.success({
          message: 'Email updated',
        })
      })
      .catch(err => {
        let message = 'Email failed to update'
        let description = null

        if (typeof err.response.data.message === 'string') {
          if (err.response.data.message === 'invalid_password') {
            message = 'Email failed to update'
            description = 'Your password is invalid'
          }
        } else {
          description = 'That email is already taken'
        }

        notification.error({ message, description })
        this.setState({ emailLoading: false })
      })
  }

  handleSubmitPassword = values => {
    const {
      user: { uuid },
    } = this.props

    this.setState({ passwordLoading: true })

    this.service
      .updatePassword(uuid, values)
      .then(() => {
        this.setState({ passwordLoading: false })
        notification.success({
          message: 'Password updated',
        })
      })
      .catch(err => {
        let message = "We couldn't to update your password"
        let description = null

        if (typeof err.response.data.message === 'string') {
          if (err.response.data.message === 'invalid_password') {
            message = 'PasswordEmail failed to update'
            description = 'Your enter invalid current password'
          }
        } else {
          description = 'That email is already taken'
        }

        notification.error({ message, description })
        this.setState({ passwordLoading: false })
      })
  }

  render() {
    const { user } = this.props
    const { emailLoading, passwordLoading } = this.state

    if (user.loading) {
      return (
        <div style={{ textAlign: 'center' }}>
          <Space size="middle">
            <Spin size="large" />
          </Space>
        </div>
      )
    }

    return (
      <Space direction="vertical">
        <Form
          onFinish={this.handleSubmitEmail}
          className="update-email-form"
          layout="vertical"
          initialValues={{
            email: user.email,
          }}
        >
          <h5 className="mb-3 text-black">
            <strong>Email</strong>
          </h5>
          <div className="row">
            <div className="col-lg-4">
              <Form.Item name="email" label="Email" rules={[{ required: true, message: ' ' }]}>
                <Input />
              </Form.Item>
            </div>

            <div className="col-lg-4">
              <Form.Item
                name="password"
                label="Password"
                rules={[{ required: true, message: ' ' }]}
              >
                <Input.Password />
              </Form.Item>
            </div>
          </div>
          <Button
            style={{ width: 150 }}
            type="primary"
            htmlType="submit"
            className="mr-3"
            loading={emailLoading}
          >
            Update Email
          </Button>
        </Form>

        <Form
          onFinish={this.handleSubmitPassword}
          className="update-email-form"
          layout="vertical"
          initialValues={{
            email: user.email,
          }}
        >
          <h5 className="mb-3 text-black">
            <strong>Password</strong>
          </h5>
          <div className="row">
            <div className="col-lg-4">
              <Form.Item
                name="current_password"
                label="Current Password"
                rules={[{ required: true, message: ' ' }]}
              >
                <Input.Password />
              </Form.Item>
            </div>

            <div className="col-lg-4">
              <Form.Item
                name="password"
                label="New Password"
                rules={[{ required: true, message: ' ' }]}
              >
                <Input.Password />
              </Form.Item>
            </div>

            <div className="col-lg-4">
              <Form.Item
                name="password_confirmation"
                label="Confirm New Password"
                rules={[{ required: true, message: ' ' }]}
              >
                <Input.Password />
              </Form.Item>
            </div>

            <Button
              style={{ width: 150 }}
              type="primary"
              htmlType="submit"
              className="mr-3"
              loading={passwordLoading}
            >
              Update Password
            </Button>
          </div>
        </Form>
      </Space>
    )
  }
}
