import React from 'react'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { ucwords } from 'services/helpers'
import { Tabs, Skeleton } from 'antd'
import store from 'store'
import Avatar from 'components/CleanUIComponents/Avatar'
import FacultyService from 'services/FacultyService'
import ProfileService from 'services/ProfileService'
// import SettingsForm from './SettingsForm'
import PersonalForm from './PersonalForm'
import AccountForm from './AccountForm'
import style from './style.module.scss'

const { TabPane } = Tabs

@connect(({ user }) => ({ user }))
class ProfileApp extends React.Component {
  constructor(props) {
    super(props)
    this.service = new FacultyService()

    this.state = {
      loading: true,
    }

    this.service = new ProfileService()
  }

  componentDidMount() {
    this.setState({ loading: true })
    const { user } = this.props

    this.service
      .show(user.uuid)
      .then(res => {
        const { roles, permissions } = res

        const state = { ...this.state, ...res }
        state.roles = roles
        state.permissions = permissions
        state.loading = false
        this.setState({ ...state })
      })
      .catch(() => {
        this.setState({ loading: false })
      })
  }

  getName() {
    const { loading, first_name: firstName, last_name: lastName } = this.state

    if (loading) {
      return <Skeleton.Input style={{ width: '300px' }} active="true" size="default" />
    }

    return (
      <strong>
        {firstName} {lastName}
      </strong>
    )
  }

  getRoles() {
    const { loading, roles } = this.state

    if (loading) {
      return <Skeleton.Input style={{ width: '300px' }} active="true" size="default" />
    }

    const r = []

    if (roles) {
      roles.forEach((role, key) => {
        const i = key + 1

        return r.push(<span key={i}>{ucwords(role)}</span>)
      })
    }

    return <p className="mb-1">{r}</p>
  }

  render() {
    const token = store.get('token')

    const {
      uuid,
      loading,
      email,
      first_name: firstName,
      last_name: lastName,
      avatar,
      middle_name: middleName,
      dob,
      status,
      gender,
      birth_place: birthPlace,
      religion,
      nationality,
      address_1: address1,
      address_2: address2,
      city,
      state,
      zipcode,
      country,
    } = this.state

    const personalFormData = {
      uuid,
      loading,
      firstName,
      lastName,
      middleName,
      dob,
      status,
      gender,
      birthPlace,
      religion,
      nationality,
      address1,
      address2,
      city,
      state,
      zipcode,
      country,
    }

    const accountForm = { uuid, email }

    const props = {
      name: 'avatar',
      action: `${
        this.service.axiosAPI.defaults.baseURL
      }${this.service.apiURL()}/upload-avatar/${uuid}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      onChange: info => {
        if (info.file.status !== 'uploading') {
          console.log(info.file, info.fileList)
        }
        // if (info.file.status === 'done') {
        //   message.success(`${info.file.name} file uploaded successfully`);
        // } else if (info.file.status === 'error') {
        //   message.error(`${info.file.name} file upload failed.`);
        // }
      },
      multiple: false,
    }

    return (
      <div>
        <Helmet title="Profile" />
        <div className={style.profile}>
          <div className="row">
            <div className="col-xl-4">
              <div className={`card ${style.header}`}>
                <div>
                  <div className="card-body text-center">
                    <div className={style.avatarCard}>
                      <Avatar
                        loading={loading}
                        name={{
                          firstName,
                          lastName,
                        }}
                        avatar={avatar}
                        uploadConfig={props}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-8">
              <div className={`card card-body mb-4 ${style.socialInfo}`}>
                <div>
                  <h2>
                    <span className="text-black mr-2">{this.getName()}</span>
                  </h2>
                  <div>{this.getRoles()}</div>
                </div>
              </div>
              <div className="card">
                <div className="card-body">
                  <Tabs defaultActiveKey="1">
                    <TabPane
                      tab={
                        <span>
                          <i className="icmn-user" /> Personal
                        </span>
                      }
                      key="1"
                    >
                      <div className="py-3">
                        <PersonalForm user={personalFormData} />
                      </div>
                    </TabPane>
                    <TabPane
                      tab={
                        <span>
                          <i className="icmn-mobile" /> Contact
                        </span>
                      }
                      key="2"
                    >
                      <div className="py-3">COntact form here</div>
                    </TabPane>
                    <TabPane
                      tab={
                        <span>
                          <i className="icmn-shield" /> Account
                        </span>
                      }
                      key="3"
                    >
                      <AccountForm user={accountForm} />
                    </TabPane>
                    {/* <TabPane
                      tab={
                        <span>
                          <i className="icmn-cog" /> Settings
                        </span>
                      }
                      key="4"
                    >
                      <SettingsForm />
                    </TabPane> */}
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ProfileApp
