import actions from './actions'

const initialState = {
  // DRAWER
  drawerState: false, // true or false for open or close

  // TABLE / LIST
  reloadListState: false,
  listLoading: false,
  dataList: [],
  pagination: {
    // Used for display
    total: 0,
    count: 0,
    current_page: 1,
    per_page: 15,
    total_pages: 0,
  },

  // Filters`
  filters: {
    orderBy: 'created_at',
    sortedBy: 'desc',
    search: '',
  },

  // Selected Event
  loadingSelectedRecord: false,
  selectedRecord: null,

  // Form-related
  formReference: null,
  viewMode: 'view', // new/edit/view/delete
  formTouched: false,
  formTriggerSave: false,
  formConfirmCancelModal: false,
}

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE:
      return { ...state, ...action.payload }
    default:
      return state
  }
}
