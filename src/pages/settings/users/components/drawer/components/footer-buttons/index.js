import React, { useEffect, useState } from 'react'
import { Button, Modal } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from '../../../../config'
import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
    setContentViewMode: viewMode =>
      dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    deleteRecord: id => dispatch({ type: `${STORE_PREFIX}/API_DELETE_RECORD`, id }),
  }
}

const FooterButtonsComponent = ({
  [STORE_PREFIX]: { drawerState, selectedRecord, viewMode, formReference, formTouched },
  formCancel,
  deleteRecord,
  setContentViewMode,
  commitRecord,
}) => {
  const [modalConfirmDeleteState, setModalConfirmDeleteState] = useState(0)

  useEffect(() => {}, [drawerState, selectedRecord])

  const showDeleteBtn = () => {
    return (
      <>
        <Button
          type="primary"
          className="cta-btn-delete mr-2"
          onClick={() => {
            setModalConfirmDeleteState(true)
          }}
        >
          Delete
        </Button>
      </>
    )
  }

  const drawerFooter = () => {
    if (viewMode === 'view' && selectedRecord && selectedRecord.id) {
      return (
        <div className="text-center">
          <Button
            type="primary"
            className="cta-btn-edit mr-2"
            onClick={() => {
              setContentViewMode('edit')
            }}
          >
            Edit this record
          </Button>

          {showDeleteBtn()}

          <Button
            type="primary"
            onClick={() => {
              formCancel()
            }}
          >
            Cancel
          </Button>
        </div>
      )
    }

    return (
      <div className="text-center">
        <Button
          type="primary"
          className="cta-btn-edit mr-2"
          htmlType="submit"
          form={formReference}
          disabled={!formTouched}
          onClick={() => {
            console.log(formReference.getFieldValue('name'))
            formReference
              .validateFields()
              .then(values => {
                formReference.resetFields()
                console.log(values)
                // onCreate(values);
                commitRecord(values)
              })
              .catch(info => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Save this record
        </Button>

        {/* <Button
          type="primary"
          className="cta-btn-delete mr-2"
          onClick={() => {
            setModalConfirmDeleteState(true)
          }}
        >
          Delete
        </Button> */}

        <Button
          type="primary"
          onClick={() => {
            formCancel()
          }}
        >
          Cancel
        </Button>
      </div>
    )
  }

  return (
    <>
      {drawerFooter()}
      <Modal
        title="Confirm"
        visible={modalConfirmDeleteState}
        onOk={() => {
          // formCancel(true)

          // - CALL API
          // - close this modal
          // - cancelForm
          // - refresh table
          deleteRecord(selectedRecord.id)
          setModalConfirmDeleteState(false)
          formCancel({ force: true })
        }}
        onCancel={() => {
          setModalConfirmDeleteState(false)
        }}
        okText="Yes"
        cancelText="No"
      >
        <p>Are you sure you want to delete this record?</p>
      </Modal>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FooterButtonsComponent)
