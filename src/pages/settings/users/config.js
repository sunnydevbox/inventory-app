/* V1.0 */
import Service from 'services/UserService'

export const LABEL_SINGULAR = 'User'
export const LABEL_PLURAL = 'Users'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featureUser'
export const API_SERVICE = Service
