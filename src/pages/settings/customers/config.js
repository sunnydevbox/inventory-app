/* V1.1 */
import Service from 'services/CustomerService'

export const LABEL_SINGULAR = 'Customer'
export const LABEL_PLURAL = 'Customers'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featureBread'
export const API_SERVICE = Service
