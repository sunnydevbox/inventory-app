import React from 'react'
import { Skeleton } from 'antd'
import { connect } from 'react-redux'
// import ItemsComponent from './items'
// import { renderField } from 'services/helpers'
import { STORE_PREFIX } from '../../../../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const ViewRecordDetailsComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
}) => {
  return (
    <>
      <div className="container pt-3">
        {loadingSelectedRecord || !selectedRecord || !selectedRecord.id ? (
          <Skeleton active />
        ) : (
          <>
            <div className="container">
              <div className="row">
                <div className="col-lg-4">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">General Information</h5>
                      <hr />
                      <div className="row">
                        <div className="col-lg-12">
                          <h3>{selectedRecord.company_name}</h3>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-lg-6">
                          {selectedRecord.first_name} {selectedRecord.last_name}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Address</h5>
                      <hr />
                      {selectedRecord.address1} {selectedRecord.city}
                      {selectedRecord.state} {selectedRecord.zip_cde}
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Contact Info</h5>
                      <hr />
                      <div className="row">
                        <div className="col-lg-12">
                          <div>Email: {selectedRecord.contact_email}</div>

                          <div>Email: {selectedRecord.contact_phone}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className="row">
                <div className="col-lg-12">
                  <div className="card">
                    <div className="card-body">
                      <ItemsComponent />
                    </div>
                  </div>
                </div>
              </div> */}
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default connect(mapStateToProps)(ViewRecordDetailsComponent)
