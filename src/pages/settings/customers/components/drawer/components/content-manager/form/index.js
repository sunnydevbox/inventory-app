/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import {
  Input,
  Form,
  Spin,
  // Button,
} from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../config'

const FormItem = Form.Item
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    getList: data => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    setFormTouched: formTouched => dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    setFormReference: formReference => dispatch({ type: `${STORE_PREFIX}/FORM_SET_REFERENCE`, formReference }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: {
    selectedRecord,
    loadingSelectedRecord,
    drawerState,
    formTriggerSave,
    formReference,
  },
  commitRecord,
  setFormTouched,
  setFormReference,
  // toggleDrawer,
}) => {
  const [form] = Form.useForm()

  useEffect(() => {
    setFormReference(form)

    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      // populate form
      // console.log(selectedRecord)
      form.setFieldsValue({
        company_name: selectedRecord.company_name,
        first_name: selectedRecord.first_name,
        last_name: selectedRecord.last_name,
        address1: selectedRecord.address1,
        address2: selectedRecord.address2,
        city: selectedRecord.city,
        contact_email: selectedRecord.contact_email,
        contact_fax: selectedRecord.contact_fax,
        contact_name: selectedRecord.contact_name,
        contact_phone: selectedRecord.contact_phone,
        contact_title: selectedRecord.contact_title,
        country: selectedRecord.country,
        state: selectedRecord.state,
        zip_code: selectedRecord.zip_code,
      })
    } else {
      form.setFieldsValue({
        company_name: form.getFieldValue('company_name'),
        address1: form.getFieldValue('address1'),
        address2: form.getFieldValue('address2'),
        last_name: form.getFieldValue('last_name'),
        first_name: form.getFieldValue('first_name'),
        city: form.getFieldValue('city'),
        contact_email: form.getFieldValue('contact_email'),
        contact_fax: form.getFieldValue('contact_fax'),
        contact_name: form.getFieldValue('contact_name'),
        contact_phone: form.getFieldValue('contact_phone'),
        contact_title: form.getFieldValue('contact_title'),
        country: form.getFieldValue('country'),
        state: form.getFieldValue('state'),
        zip_code: form.getFieldValue('zip_code'),
      })
    }

    // if (formTriggerSave) {
    //   console.log(form.getFieldValue('name'))
    //   form
    //       .validateFields()
    //       .then(values => {
    //         // form.resetFields();
    //         commitRecord(values)
    //       })
    //       .catch(info => {
    //         console.log('Validate Failed:', info);
    //       });
    // }
  }, [
    form,
    setFormReference,
    drawerState,
    selectedRecord,
    loadingSelectedRecord,
    formTriggerSave,
    commitRecord,
    formReference,
  ])

  const handleSave = async values => {
    commitRecord(values)
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        layout="vertical"
        form={form}
        onFinish={handleSave}
        onFieldsChange={() =>
        // changedValues
        {
          console.log(11)
          setFormTouched(true)
        }
        }
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-8">
              <div className="row">
                <div className="col-lg-12">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">General Information</h5>
                      <hr />
                      <div className="row">
                        <div className="col-lg-12">
                          <FormItem
                            name="company_name"
                            label="Company"
                            rules={[
                              { message: 'Enter name' },
                              { min: 3, message: 'Minimum of 3 characters' },
                            ]}
                            extra="Company name or person's name"
                          >
                            <Input />
                          </FormItem>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-lg-6">
                          <FormItem
                            name="first_name"
                            label="First Name"
                            rules={[
                              { required: true, message: 'Enter First Name' },
                              { min: 3, message: 'Minimum of 3 characters' },
                            ]}
                          >
                            <Input />
                          </FormItem>
                        </div>
                        <div className="col-lg-6">
                          <FormItem
                            name="last_name"
                            label="Last Name"
                            rules={[
                              { required: true, message: 'Enter Last Name' },
                              { min: 3, message: 'Minimum of 3 characters' },
                            ]}
                          >
                            <Input />
                          </FormItem>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Address</h5>
                      <hr />

                      <FormItem name="address1" label="Address">
                        <Input placeholder="Address" />
                      </FormItem>

                      <div className="row">
                        <div className="col-lg-4">
                          <FormItem name="city" label="City">
                            <Input placeholder="City" />
                          </FormItem>
                        </div>

                        <div className="col-lg-4">
                          <FormItem name="state" label="State">
                            <Input placeholder="State" />
                          </FormItem>
                        </div>

                        <div className="col-lg-4">
                          <FormItem name="zip_code" label="Zip Code">
                            <Input placeholder="Zip Code" />
                          </FormItem>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">Contact Info</h5>
                  <hr />
                  <div className="row">
                    <div className="col-lg-12">
                      <FormItem
                        label="Email"
                        name="contact_email"
                        rules={[{ type: 'email', message: `Email is not a valid!` }]}
                      >
                        <Input placeholder="Email" />
                      </FormItem>
                    </div>

                    <div className="col-lg-12">
                      <FormItem
                        label="Contact Number"
                        name="contact_phone"
                        rules={[{ required: true }]}
                      >
                        <Input placeholder="Phone" />
                      </FormItem>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Form>
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
