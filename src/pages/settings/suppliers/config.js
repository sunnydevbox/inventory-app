/* V1.1 */
import Service from 'services/SupplierService'

export const LABEL_SINGULAR = 'Supplier'
export const LABEL_PLURAL = 'Suppliers'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featureBread'
export const API_SERVICE = Service
