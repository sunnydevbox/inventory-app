/* V1.0 */
import Service from 'services/CourierService'

export const SERIVCE_PATH = 'services/CourierService'
export const LABEL_SINGULAR = 'Courier'
export const LABEL_PLURAL = 'Couriers'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featureBread'
export const API_SERVICE = Service
