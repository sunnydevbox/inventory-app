/* V1.0 */
import Service from 'services/MetricService'

export const LABEL_SINGULAR = 'Unit'
export const LABEL_PLURAL = 'Units'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featureBread'
export const API_SERVICE = Service
