import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import BreadComponent from 'components/bread'
import FormComponent from './components/drawer/components/content-manager/form'
import ViewRecordDetailsComponent from './components/drawer/components/content-manager/view-record-details'
import * as config from './config'

const FeatureComponent = ({
  ...props
}) => {

  return (
    <>
      <BreadComponent
        columns={[
          {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
          },
        ]}
        config={config}
        content={{
          view: <ViewRecordDetailsComponent />,
          form: <FormComponent />
        }}
        {...props}
      />
    </>
  )
}

export default withRouter(connect()(FeatureComponent))
