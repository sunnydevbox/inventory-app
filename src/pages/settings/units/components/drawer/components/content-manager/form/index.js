/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import {
  Input,
  Form,
  Spin,
  // Button,
} from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../config'

const FormItem = Form.Item
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: data => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER`, data }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    getList: data => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST`, data }),
    resetSelectedRecord: () => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id: null }),
    setFormTouched: formTouched => dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    setFormReference: formReference => dispatch({ type: `${STORE_PREFIX}/FORM_SET_REFERENCE`, formReference }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: {
    selectedRecord,
    loadingSelectedRecord,
    drawerState,
    formTriggerSave,
    formReference,
  },
  commitRecord,
  setFormTouched,
  setFormReference,
  // toggleDrawer,
}) => {
  const [form] = Form.useForm()

  useEffect(() => {
    setFormReference(form)

    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      // populate form
      // console.log(selectedRecord)
      form.setFieldsValue({
        symbol: selectedRecord.symbol,
        name: selectedRecord.name,
      })
    } else {
      form.setFieldsValue({
        name: form.getFieldValue('name'),
        symbol: form.getFieldValue('symbol'),
      })
    }

    // if (formTriggerSave) {
    //   console.log(form.getFieldValue('name'))
    //   form
    //       .validateFields()
    //       .then(values => {
    //         // form.resetFields();
    //         commitRecord(values)
    //       })
    //       .catch(info => {
    //         console.log('Validate Failed:', info);
    //       });
    // }
  }, [
    form,
    setFormReference,
    drawerState,
    selectedRecord,
    loadingSelectedRecord,
    formTriggerSave,
    commitRecord,
    formReference,
  ])

  const handleSave = async values => {
    commitRecord(values)
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        layout="vertical"
        form={form}
        onFinish={handleSave}
        onFieldsChange={() =>
          // changedValues
          {
            setFormTouched(true)
          }
        }
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-6 offset-lg-3">
              <div className="row">
                <div className="col-lg-12">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Details</h5>
                      <hr />
                      <FormItem
                        name="name"
                        label="Name"
                        rules={[
                          { required: true, message: 'Enter Name' },
                          { min: 1, message: 'Minimum of 1   characters' },
                        ]}
                      >
                        <Input />
                      </FormItem>

                      <FormItem
                        name="symbol"
                        label="Symbol"
                        rules={[
                          { required: true, message: 'Enter Symbol' },
                          { min: 1, message: 'Minimum of 1 characters' },
                        ]}
                      >
                        <Input />
                      </FormItem>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Form>
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
