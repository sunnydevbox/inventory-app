/* V1.0 */
import Service from 'services/PaymentTermService'

export const LABEL_SINGULAR = 'Payment Term'
export const LABEL_PLURAL = 'Payment Terms'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featureBread'
export const API_SERVICE = Service
