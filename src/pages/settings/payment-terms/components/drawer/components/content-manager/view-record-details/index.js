import React from 'react'
import { Skeleton } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from 'pages/settings/payment-terms/config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const ViewRecordDetailsComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
}) => {
  return (
    <>
      <div className="container pt-3">
        {loadingSelectedRecord || !selectedRecord || !selectedRecord.id ? (
          <Skeleton active />
        ) : (
          <>
            <div className="container">
              <div className="row">
                <div className="col-lg-6 offset-lg-3">
                  <div className="card">
                    <div className="card-body">
                      {selectedRecord.name}({selectedRecord.symbol})
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default connect(mapStateToProps)(ViewRecordDetailsComponent)
