import React from 'react'
import { Skeleton } from 'antd'
import { connect } from 'react-redux'
// import { renderField } from 'services/helpers'
import parser from 'html-react-parser'
import { STORE_PREFIX } from '../../../../../config'
import './styles.scss'

const mapStateToProps = (state) => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const ViewRecordDetailsComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
}) => {
  const getDescription = () => {
    const { description } = selectedRecord

    if (description && description.length) {
      return parser(`<p>${description}</p>`)
    }

    return parser('<em>Description not set</em>')
  }

  return (
    <>
      <div className="container pt-3">
        {loadingSelectedRecord || !selectedRecord || !selectedRecord.id ? (
          <Skeleton active />
        ) : (
          <>
            <div className="container">
              <div className="row">
                <div className="col-lg-6 offset-lg-3">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Category Details</h5>
                      Label: {selectedRecord.name}
                      <div className="description p-2 border rounded mt-2">{getDescription()}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default connect(mapStateToProps)(ViewRecordDetailsComponent)
