/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import {
  Input,
  Form,
  Spin,
  // Button,
} from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import './styles.scss'
import { STORE_PREFIX } from 'pages/settings/category/config'

const { TextArea } = Input;

const FormItem = Form.Item
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    setFormTouched: formTouched => dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    setFormReference: formReference => dispatch({ type: `${STORE_PREFIX}/FORM_SET_REFERENCE`, formReference }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: {
    selectedRecord,
    loadingSelectedRecord,
    drawerState,
    formTriggerSave,
    formReference,
  },
  commitRecord,
  setFormTouched,
  setFormReference,
}) => {
  const [form] = Form.useForm()

  useEffect(() => {
    setFormReference(form)

    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      // populate form
      console.log(selectedRecord)
      form.setFieldsValue({
        name: selectedRecord.name,
        description: selectedRecord.description,
      })
    } else {
      form.setFieldsValue({
        name: form.getFieldValue('name'),
        description: form.getFieldValue('description'),
      })
    }
  }, [
    form,
    setFormReference,
    drawerState,
    selectedRecord,
    loadingSelectedRecord,
    formTriggerSave,
    commitRecord,
    formReference,
  ])

  // const handleSave = async values => {
  //   commitRecord(values)
  // }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        layout="vertical"
        form={form}
        // onFinish={handleSave}
        onFieldsChange={() =>
          // changedValues
          {
            setFormTouched(true)
          }
        }
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-6 offset-lg-3">
              <div className="row">
                <div className="col-lg-12">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Category Details</h5>
                      <hr />
                      <FormItem
                        name="name"
                        label="Name"
                        rules={[
                          { required: true, message: 'Enter Name' },
                          { min: 1, message: 'Minimum of 1   characters' },
                        ]}
                      >
                        <Input />
                      </FormItem>

                      <FormItem
                        name="description"
                        help="Enter a description of this category"
                      >
                        <TextArea rows={4} />
                      </FormItem>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Form>
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
