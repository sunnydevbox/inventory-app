/* V1.0 */
import Service from 'services/CategoryService'

export const SERIVCE_PATH = 'services/CategoryService'
export const LABEL_SINGULAR = 'Category'
export const LABEL_PLURAL = 'Categories'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featureBread'
export const API_SERVICE = Service
