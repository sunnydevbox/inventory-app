import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import BreadComponent from 'components/bread'
import FormComponent from './components/drawer/components/content-manager/form'
import ViewRecordDetailsComponent from './components/drawer/components/content-manager/view-record-details'
import * as config from './config'
import './styles.scss'

const FeatureComponent = ({
  ...props
}) => {

  return (
    <>
      <BreadComponent
        columns={[
          {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
          },
          {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
            className: 'description',
          },
        ]}
        config={config}
        content={{
          view: <ViewRecordDetailsComponent />,
          form: <FormComponent />
        }}
        {...props}
      />
    </>
  )
}

export default withRouter(connect()(FeatureComponent))
