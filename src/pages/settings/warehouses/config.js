/* V1.1 */
import Service from 'services/LocationService'

export const LABEL_SINGULAR = 'Warehouse'
export const LABEL_PLURAL = 'Warehouses'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featureBread'
export const API_SERVICE = Service
