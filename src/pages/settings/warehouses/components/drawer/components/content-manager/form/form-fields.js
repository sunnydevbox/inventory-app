const fields = {
  name: {
    rules: [
      { required: true, message: 'Enter name' },
      { min: 3, message: 'Minimum of 3 characters' },
    ],
  },
  description: {
    rules: [
      { required: true, message: 'Enter name' },
      { min: 3, message: 'Minimum of 3 characters' },
    ],
  },
}

export default fields
