import React, { useEffect, useState } from 'react'
import { Button, Modal } from 'antd'
import { connect } from 'react-redux'
import PurchaseOrderService from 'services/PurchaseOrderService'
import { Notification } from 'services/helpers'
import { STORE_PREFIX } from 'pages/purchase-orders/config'
import './styles.scss'

const mapStateToProps = (state) => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = (dispatch) => {
  return {
    formCancel: (force) => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
    setContentViewMode: (viewMode) =>
      dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),
    commitRecord: async (data) => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    deleteRecord: (id) => dispatch({ type: `${STORE_PREFIX}/API_DELETE_RECORD`, id }),
    selectRecord: (id) => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id }),
    getList: () => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST` }),
  }
}

const FooterButtonsComponent = ({
  [STORE_PREFIX]: { drawerState, selectedRecord, viewMode, formReference, formTouched },
  formCancel,
  deleteRecord,
  setContentViewMode,
  commitRecord,
  selectRecord,
  getList,
}) => {
  const [modalConfirmDeleteState, setModalConfirmDeleteState] = useState(0)

  useEffect(() => {}, [drawerState, selectedRecord])

  const showDeleteBtn = () => {
    return (
      <>
        <Button
          type="primary"
          className="cta-btn-delete mr-2"
          onClick={() => {
            setModalConfirmDeleteState(true)
          }}
        >
          Delete
        </Button>
      </>
    )
  }

  const doActionBtn = (state) => {
    PurchaseOrderService.setState(selectedRecord.id, state)
      .then(() => {
        selectRecord(selectedRecord.id)
        getList()
        Notification({
          status: 200,
          title: 'Saved',
        })
      })
      .catch((error) => {
        Notification(error)
        console.log(error)
      })
  }

  const actionBtn = () => {
    /*
    STATE_PO_UNAPPROVED       = 'unapproved';
    const STATE_PO_PARKED           = 'parked';
    const STATE_PO_PLACED           = 'placed';
    const STATE_PO_COSTED           = 'costed';
    const STATE_PO_RECEIPTED        = 'receipted';
    const STATE_PO_PARTIAL_SERVED   = 'partial_served';
    const STATE_PO_COMPLETED        = 'complete';
    const STATE_PO_DELETED          = 'deleted';
    */

    if (selectedRecord) {
      const { status, items_count: itemsCount } = selectedRecord

      // CHECK PERMISSION TOO

      if (status === 'parked') {
        return (
          <>
            <Button
              type="primary"
              className="cta-btn-edit mr-2"
              onClick={() => {
                setContentViewMode('edit')
              }}
            >
              Edit this record
            </Button>

            <Button
              type="primary"
              className="cta-btn-edit mr-2"
              disabled={!itemsCount}
              onClick={() => {
                doActionBtn('costing')
              }}
            >
              Submit for Costing
            </Button>

            {showDeleteBtn()}
          </>
        )
      }

      if (status === 'costing') {
        return (
          <>
            <Button
              type="primary"
              className="cta-btn-edit mr-2"
              onClick={() => {
                doActionBtn('unapproved')
              }}
            >
              Disapprove
            </Button>

            <Button
              type="primary"
              className="cta-btn-edit mr-2"
              onClick={() => {
                doActionBtn('costed')
              }}
            >
              Costed
            </Button>
          </>
        )
      }

      if (status === 'placed' || status === 'place_order' || status === 'partial_served') {
        return (
          <>
            <Button
              type="primary"
              className="cta-btn-edit mr-2"
              onClick={() => {
                setContentViewMode('edit')
              }}
            >
              Completed
            </Button>
          </>
        )
      }

      if (status === 'costed') {
        return (
          <>
            <Button
              type="primary"
              className="cta-btn-edit mr-2"
              onClick={() => {
                doActionBtn('placed')
              }}
            >
              Place Order
            </Button>
          </>
        )
      }
    }

    return null
  }

  const drawerFooter = () => {
    const btnCancel = () => (
      <Button
        type="primary"
        className="pull-right"
        onClick={() => {
          formCancel()
        }}
      >
        Cancel
      </Button>
    )

    if (viewMode === 'view' && selectedRecord && selectedRecord.id) {
      return (
        <div className="text-center">
          {actionBtn()}

          {btnCancel()}
        </div>
      )
    }

    return (
      <div className="text-center">
        <Button
          type="primary"
          className="cta-btn-edit mr-2"
          htmlType="submit"
          form={formReference}
          disabled={!formTouched}
          onClick={() => {
            formReference
              .validateFields()
              .then((values) => {
                // onCreate(values);
                commitRecord(values)

                // if (!formCommitError) {
                //   formReference.resetFields()
                // }
              })
              .catch((info) => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Save
        </Button>

        {btnCancel()}
      </div>
    )
  }

  return (
    <>
      {drawerFooter()}
      <Modal
        title="Confirm"
        visible={modalConfirmDeleteState}
        onOk={() => {
          // formCancel(true)

          // - CALL API
          // - close this modal
          // - cancelForm
          // - refresh table
          deleteRecord(selectedRecord.id)
          setModalConfirmDeleteState(false)
          formCancel({ force: true })
        }}
        onCancel={() => {
          setModalConfirmDeleteState(false)
        }}
        okText="Yes"
        cancelText="No"
      >
        <p>Are you sure you want to delete this record?</p>
      </Modal>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FooterButtonsComponent)
