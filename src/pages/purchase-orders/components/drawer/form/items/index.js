/* eslint-disable no-nested-ternary */
import React, { useEffect, useState, useCallback } from 'react'
import { Table, Button, Space } from 'antd'
import { connect } from 'react-redux'
import { PlusOutlined, DeleteOutlined, EditOutlined } from '@ant-design/icons'
import PurchaseOrderItemService from 'services/PurchaseOrderItemService'
import { STORE_PREFIX } from 'pages/purchase-orders/config'
import FormItemsModalAddEditComponent from './modal-addedit-item'
import FormItemsModalDeleteComponent from './modal-delete-item'
import FormItesmModalServeComponent from './modal-serve'
import './styles.scss'

const mapStateToProps = (state) => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = (dispatch) => {
  return {
    formCancel: (force) => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
    cancelFormConfirmCancelModal: async () =>
      dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL_MODAL` }),
    poBrowseReload: () => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST` }),
    selectRecord: (id) => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id }),
  }
}

const FormItemsComponent = ({
  [STORE_PREFIX]: {
    selectedRecord,
    loadingSelectedRecord,
    viewMode,
    formConfirmCancelModal,
    drawerState,
  },
  poBrowseReload,
  selectRecord,
}) => {
  const [showAddEditModal, setShowAddEditModal] = useState(false)
  const [items, setItems] = useState([])
  const [currentPOitem, setCurrentPOitem] = useState(null)
  const [loading, setLoading] = useState(false)
  const [showServeModal, setShowServeModal] = useState(false)
  const [newColumns, setNewColumns] = useState([])
  const [showDeleteItemModal, setShowDeleteItemModal] = useState(false)
  const [status, setStatus] = useState(null)

  const getList = useCallback(() => {
    setLoading(true)
    PurchaseOrderItemService.index({
      search: `purchase_order_id:${selectedRecord.id}`,
      limit: 0,
    })
      .then((response) => {
        setItems(response.data)
        setLoading(false)
      })
      .catch((err) => {
        console.log(err)
        setLoading(false)
      })
  }, [selectedRecord])

  useEffect(() => {
    if (selectedRecord && selectedRecord.id && !loadingSelectedRecord) {
      setStatus(selectedRecord.status)
      getList()
    }

    const columns = [
      {
        title: 'Name',
        dataIndex: 'item',
        key: 'name',
        render: (item) => {
          const { name, sku } = item
          return `${name} ${sku || ''}`
        },
      },
      {
        title: 'Qty',
        dataIndex: 'quantity',
        key: 'quantity',
      },
      {
        title: 'Unit Price',
        dataIndex: 'price',
        key: 'price',
      },
      {
        title: 'Amount',
        dataIndex: 'total',
        key: 'total',
      },
    ]

    setNewColumns(columns)

    if (selectedRecord) {
      if (selectedRecord.status === 'parked') {
        if (viewMode === 'edit') {
          // setNewColumns(columns)
          const col = columns
          col.push({
            // title: '',
            // dataIndex: 'action',
            key: 'action',
            render: (row) => {
              return (
                <>
                  <Space>
                    <EditOutlined
                      onClick={() => {
                        setCurrentPOitem(row)
                        setShowAddEditModal(true)
                      }}
                    />
                    <DeleteOutlined
                      onClick={() => {
                        setCurrentPOitem(row)
                        setShowDeleteItemModal(true)
                      }}
                    />
                  </Space>
                </>
              )
            },
          })

          setNewColumns(col)
        } else {
          setNewColumns(columns)
        }
      } else if (selectedRecord.status === 'placed') {
        // setNewColumns(columns)
        const col = columns
        // col = Array.merge(col, columns)
        // col = col.merge(columns)
        // console.log('p', col, columns)
        col.push({
          title: 'Served Quantity',
          dataIndex: 'served_quantity',
          key: 'served_quantity',
        })

        col.push({
          title: 'Action',
          key: 'action',
          render: (poItem) => {
            return (
              <Button
                onClick={() => {
                  setCurrentPOitem(poItem)
                  setShowServeModal(true)
                }}
              >
                Serve
              </Button>
            )
          },
        })

        setNewColumns(col)
      }
    }
  }, [
    selectedRecord,
    loadingSelectedRecord,
    formConfirmCancelModal,
    viewMode,
    drawerState,
    // columns,
    getList,
    setShowServeModal,
  ])

  const getAddButton = () => {
    if (!loading && (viewMode === 'edit' || viewMode === 'new') && status === 'parked') {
      return (
        <Button
          className="btn btn-add btn-primary"
          onClick={() => setShowAddEditModal(!showAddEditModal)}
        >
          <PlusOutlined /> Add Item
        </Button>
      )
    }
    return null
  }

  const triggerReloadList = (v) => {
    if (v) {
      selectRecord(selectedRecord.id)
      getList()
      poBrowseReload()
    }
  }

  return (
    <>
      <Table
        loading={loading}
        dataSource={items}
        columns={newColumns}
        pagination={false}
        rowKey={(row) => row.id}
        onRow={(record) => {
          return {
            onClick: () => {
              if (
                // viewMode === 'edit' ||
                viewMode === 'new'
              ) {
                setShowAddEditModal(true)
                setCurrentPOitem(record)
              }
            },
          }
        }}
      />

      {getAddButton()}

      <FormItemsModalAddEditComponent
        showModal={showAddEditModal}
        poId={selectedRecord ? selectedRecord.id : null}
        currentItem={currentPOitem}
        visiblityChanged={(v) => {
          if (!v) {
            setCurrentPOitem(null)
          }
          setShowAddEditModal(v)
        }}
        triggerReload={triggerReloadList}
      />

      <FormItesmModalServeComponent
        showModal={showServeModal}
        currentItem={currentPOitem}
        visiblityChanged={(v) => {
          if (!v) {
            setCurrentPOitem(null)
          }
          setShowServeModal(v)
        }}
        triggerReload={triggerReloadList}
      />

      <FormItemsModalDeleteComponent
        showModal={showDeleteItemModal}
        currentItem={currentPOitem}
        visiblityChanged={(v) => {
          if (!v) {
            setCurrentPOitem(null)
          }
          setShowDeleteItemModal(v)
        }}
        triggerReload={triggerReloadList}
      />
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormItemsComponent)
