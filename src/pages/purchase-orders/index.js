import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import parser from 'html-react-parser'
import BreadComponent from 'components/bread'
import { formatDate, poStateTranslation } from 'services/helpers'
import FormComponent from './components/drawer/form'
import ViewRecordDetailsComponent from './components/drawer/view-record-details'
import FooterButtons from './components/footer-buttons'
import * as config from './config'

const mapStateToProps = state => ({ [config.STORE_PREFIX]: state[config.STORE_PREFIX] })


const FeatureComponent = ({ 
  ...props
}) => {

  return (
    <>
      <BreadComponent
        browse={{
          columns:[
            {
              title: 'Code',
              dataIndex: 'code',
              key: 'code',
            },
        
            {
              title: 'Order Date',
              dataIndex: 'order_date',
              key: 'order_date',
              render: orderDate => {
                if (orderDate) {
                  return formatDate(orderDate)
                }
  
                return parser('<em>Not Set</em>')
              },
            },
        
            {
              title: 'Supplier',
              dataIndex: 'supplier',
              key: 'supplier',
              render: supplier => {
                if (supplier) {
                  return supplier.name
                }
                return parser('<em>Not Set</em>')
              },
            },
        
            {
              title: 'Items Count',
              dataIndex: 'items_count',
              key: 'items_count',
            },
        
            {
              title: 'Status',
              dataIndex: 'status',
              key: 'status',
              render: status => {
                return parser(`<span style="text-transform:uppercase">${poStateTranslation(status)}</span>`)
              }
            },
          ],
          actions: {
            edit: {
              show: (row) => {
                if (row.status === 'parked') {
                  return true
                }
                return false
              }
            },
            view: {
              show: () => {
                return true
                // if (row.status !== 'parked') {
                //   return true
                // }
                // return false
              }
            },
            destroy: {
              show: (row) => {
                if (row.status === 'parked') {
                  return true
                }
                return false
              }
            }
          }
        }}
        config={config}
        content={{
          view: <ViewRecordDetailsComponent />,
          form: <FormComponent />,
          overrideButtons: <FooterButtons />
        }}
        {...props}
      />
    </>
  )
}

export default withRouter(connect(mapStateToProps)(FeatureComponent))
