import React from 'react'

class Forbidden extends React.Component {
  render() {
    return (
      <div>
        <section className="card">
          <section className="card-body">
            <h1>Permission Denied</h1>
          </section>
          <section className="card-body">
            Sorry! You do not have permission to access this page.
          </section>
        </section>
      </div>
    )
  }
}

export default Forbidden
