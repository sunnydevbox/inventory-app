import parser from 'html-react-parser'

const columns = [
  {
    title: 'SKU',
    dataIndex: 'sku',
    key: 'sku',
    render: sku => {
      if (!sku) {
        return parser('<em>Not Set</em>')
      }
      return sku
    },
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: name => {
      return name
    },
  },
  {
    title: 'Supplier',
    dataIndex: 'suppliers',
    key: 'suppliers',
    render: suppliers => {
      if (suppliers && suppliers.length) {
        const s = []
        suppliers.forEach(supplier => {
          s.push(`<div>${supplier.name}</div>`)
        })

        return parser(s.join(''))
      }
      return parser('<em>Not Set</em>')
    },
  },

  // {
  //   title: 'Warehouse',
  //   dataIndex: 'location',
  //   key: 'warehouse',
  //   render: row => {
  //     return row.name
  //   },
  // }

  // {
  //   title: 'Metric',
  //   dataIndex: 'metric',
  //   key: 'metric',
  //   render: metric => {
  //     if (metric) {
  //       return `${metric.name} (${metric.symbol})`
  //     }
  //     return parser('<em>Not Set</em>')
  //   },
  // },

  {
    title: 'Category',
    dataIndex: 'category',
    key: 'category',
    render: category => {
      if (category) {
        return category.name
      }
      return parser('<em>Not Set</em>')
    },
  },

  {
    title: 'Stocks',
    dataIndex: 'stock_count',
    key: 'stock_count',
    render: stockCount => {
      if (stockCount) {
        return stockCount
      }
      return parser('<em>0</em>')
    },
  },

  {
    title: 'Expires?',
    dataIndex: 'has_expiration',
    key: 'has_expiration',
    render: expires => {
      return expires ? 'Yes' : 'No'
      // if ()
      // return parser('<em>Not Set</em>')
    },
  },

  // {
  //   title: 'Supplier',
  //   dataIndex: 'item',
  //   key: 'supplier_id',
  //   render: row => {
  //     const suppliers = []

  //     if (row.suppliers) {
  //       row.suppliers.forEach(supplier => {
  //           suppliers.push(supplier.name)
  //       });
  //     }

  //     if (suppliers.length) {
  //       return suppliers.join(',')
  //     }

  //     return parser('<em>None  Specified</em>')
  //   },
  // },

  // {
  //   title: 'Storage',
  //   dataIndex: 'location',
  //   key: 'location',
  //   render: row => {
  //     if (row) {
  //       return row.name
  //     }

  //     return parser('<em>Not Specified</em>')
  //   },
  // },

  // {
  //   title: 'Action',
  //   key: 'action',
  //   render: row => {
  //     return (
  //       <span>
  //         <Button
  //           type="primary"
  //           className="mr-1"
  //           size="small"
  //           onClick={() => {
  //             toggleDrawer()
  //             setRecord(row.id)
  //           }}
  //         >
  //           View
  //         </Button>

  //         {/* <Button size="small" danger onClick={() => this.confirm(record.id)}>
  //           Remove
  //         </Button> */}
  //       </span>
  //     )
  //   },
  // },
]

export default columns
