import React, { useCallback, useEffect, useState } from 'react'
import { Space, Table } from 'antd'
import { connect } from 'react-redux'
import RefreshButtonComponent from 'components/btn-refresh'
import SearchbarComponent from 'components/form/searchbar'
// import FiltersComponent from './components/filters'
import './styles.scss'
import { STORE_PREFIX } from '../../config'
import columns from './columns'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }),
    getList: () => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST` }),
    setFilter: filters => dispatch({ type: `${STORE_PREFIX}/SET_FILTER`, filters }),
    setRecord: id => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id }),
    setSearchTerm: search => dispatch({ type: `${STORE_PREFIX}/SET_SEARCH_TERM`, search }),
  }
}

const FeatureBrowseComponent = ({
  [STORE_PREFIX]: { dataList, listLoading, pagination },
  toggleDrawer,
  setFilter,
  setRecord,
  getList,
  setSearchTerm,
}) => {
  const [firstLoad, setFirstLoad] = useState(true)

  const trig = useCallback(() =>
    // paginatipon,
    // filters,
    // sorter,
    // extra: {
    //   currentDataSource: [],
    //   action: paginate | sort | filter
    // }
    {
      // console.log(paginatipon, filters, sorter)
      setFilter()
    }, [setFilter])

  useEffect(() => {
    if (firstLoad) {
      setFirstLoad(false)
      trig()
    }
  }, [firstLoad, trig])

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        {/* <RefreshButtonComponent callback={getList} loading={listLoading} /> */}
        <div className="row">
          <div className="col-sm-11">
            <Space>
              <SearchbarComponent
                searchLoading={listLoading}
                callback={setSearchTerm}
                minChar={0}
                placeholder="Name/SKU/Category"
              />
              {/* <FiltersComponent /> */}
            </Space>
          </div>
          <div className="col-sm-1">
            <RefreshButtonComponent
              style={{ float: 'right' }}
              callback={getList}
              loading={listLoading}
            />
          </div>
        </div>
        <Table
          loading={listLoading}
          dataSource={dataList}
          columns={columns}
          rowKey={row => row.id}
          onChange={(pa, filters, sorter) => {
            trig(pa, filters, sorter)
          }}
          pagination={{
            showTotal: total => `Total ${total} items`,
            total: pagination.total,
            defaultPageSize: pagination.per_page,
            onChange: (page, pageSize) => {
              setFilter({
                page,
                limit: pageSize,
              })
            },
            position: ['bottomCenter'],
          }}
          onRow={record => {
            return {
              onClick: () => {
                toggleDrawer()
                setRecord(record.id)
              },
            }
          }}
        />
      </Space>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FeatureBrowseComponent)
