import React, { useEffect, useState } from 'react'
import { Select } from 'antd'
import { connect } from 'react-redux'
// import DropDown from 'components/form/dropdown-search-data'
import DropdownMultipleSelectComponent from 'components/form/dropdown-multiple-select'
import { STORE_PREFIX } from '../../../../config'

// const { Option }= Select

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX], settings: state.settings })
const mapDispatchToProps = dispatch => {
  return {
    setFilter: filters => dispatch({ type: `${STORE_PREFIX}/SET_FILTER`, filters }),
    resetFilters: () => dispatch({ type: `${STORE_PREFIX}/RESET_FILTERS` }),
  }
}

const FiltersComponent = ({
  settings: { suppData },
  // [STORE_PREFIX]: { filters },
  setFilter,
  resetFilters,
}) => {
  // const [locations, setLocations] = useState([])
  const [selectedLocations, setSelectedLocations] = useState([])
  const [optionsLocations, setOptionsLocations] = useState(null)

  useEffect(() => {
    if (suppData && suppData.locations && !optionsLocations) {
      const a = suppData.locations.map(item => (
        <Select.Option value={item.id} label={item.name} key={`${item.name}-${item.id}`}>
          {item.name}
        </Select.Option>
      ))

      setOptionsLocations(a)
    }
  }, [suppData, setOptionsLocations, setSelectedLocations, optionsLocations, selectedLocations])

  const filterChanged = (key, value) => {
    if (key) {
      const d = []
      d[key] = value
      setFilter(d)
    } else {
      setSelectedLocations([])
      resetFilters()
    }
  }

  return (
    <>
      status,
      <DropdownMultipleSelectComponent
        defaultValue={[]}
        onChange={field => {
          setSelectedLocations(field)
          filterChanged('locations', field.join(','))
        }}
        value={selectedLocations}
        style={{ width: '100%' }}
        placeholder="Select Warehouse"
        optionLabelProp="label"
      >
        {optionsLocations}
      </DropdownMultipleSelectComponent>
      <a
        role="button"
        tabIndex={0}
        onClick={() => filterChanged(null)}
        onKeyDown={() => filterChanged(null)}
      >
        Clear Filters
      </a>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FiltersComponent)
