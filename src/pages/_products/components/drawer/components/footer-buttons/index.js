import React, { useEffect, useState } from 'react'
import { Button, Modal } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from '../../../../config'
import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
    setContentViewMode: viewMode =>
      dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    deleteRecord: id => dispatch({ type: `${STORE_PREFIX}/API_DELETE_RECORD`, id }),
  }
}

const FooterButtonsComponent = ({
  [STORE_PREFIX]: { selectedRecord, viewMode, formReference, formTouched, formTriggerSave },
  formCancel,
  deleteRecord,
  setContentViewMode,
  // commit Record,
}) => {
  const [modalConfirmDeleteState, setModalConfirmDeleteState] = useState(false)

  useEffect(() => {}, [])

  // const sub = async (values) => {
  //   await commitRecord(values)
  // }

  const doSubmit = async () => {
    formReference
      .validateFields()
      .then(() => {
        formReference.submit()
      })
      .catch(info => {
        console.log('Validate Failed:', info)
      })
  }

  const drawerFooter = () => {
    if (viewMode === 'view' && selectedRecord && selectedRecord.id) {
      return (
        <div className="text-center">
          <Button
            type="primary"
            className="cta-btn-edit mr-2"
            onClick={() => {
              setContentViewMode('edit')
            }}
          >
            Edit this record
          </Button>

          {viewMode !== 'new' ? (
            <Button
              type="primary"
              className="cta-btn-delete mr-2"
              onClick={() => {
                setModalConfirmDeleteState(true)
              }}
            >
              Delete
            </Button>
          ) : null}

          <Button
            type="primary"
            onClick={() => {
              formCancel()
            }}
          >
            Cancel
          </Button>
        </div>
      )
    }
    return (
      <div className="text-center">
        {formTriggerSave}
        <Button
          type="primary"
          className="cta-btn-edit mr-2"
          htmlType="submit"
          form={formReference}
          disabled={!formTouched}
          onClick={doSubmit}
        >
          Save this record
        </Button>

        {viewMode !== 'new' ? (
          <Button
            type="primary"
            className="cta-btn-delete mr-2"
            onClick={() => {
              setModalConfirmDeleteState(true)
            }}
          >
            Delete
          </Button>
        ) : null}

        {viewMode === 'new' || viewMode === 'edit' ? (
          <Button
            type="primary"
            className="cta-btn-delete mr-2"
            onClick={() => {
              if (formReference) {
                formReference.resetFields()
              }
            }}
          >
            Clear
          </Button>
        ) : null}

        <Button
          type="primary"
          onClick={() => {
            if (formReference) {
              formReference.resetFields()
            }
            formCancel()
          }}
        >
          Cancel
        </Button>
      </div>
    )
  }

  return (
    <>
      {drawerFooter()}
      <Modal
        title="Confirm"
        visible={modalConfirmDeleteState}
        onOk={() => {
          // formCancel(true)

          // - CALL API
          // - close this modal
          // - cancelForm\
          // - reset form fields
          // - refresh table
          deleteRecord(selectedRecord.id)
          setModalConfirmDeleteState(false)
          formCancel({ force: true })
          if (formReference) {
            formReference.resetFields()
          }
        }}
        onCancel={() => {
          setModalConfirmDeleteState(false)
        }}
        okText="Yes"
        cancelText="No"
      >
        <p>Are you sure you want to delete this record?</p>
      </Modal>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FooterButtonsComponent)
