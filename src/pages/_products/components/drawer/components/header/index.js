import React from 'react'
import { Skeleton } from 'antd'
import parser from 'html-react-parser'
import { connect } from 'react-redux'
import { LABEL_SINGULAR, STORE_PREFIX } from '../../../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const HeaderComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord, viewMode },
}) => {
  const getDrawerTitle = () => {
    let title = ''

    if (!selectedRecord && viewMode === 'new') {
      title = `${LABEL_SINGULAR}`
    } else if (selectedRecord) {
      const { name } = selectedRecord
      let label = `${LABEL_SINGULAR}: <strong>${name}</strong>`

      if (viewMode === 'edit') {
        label = `Update ${LABEL_SINGULAR}: <strong>${name}</strong>`
      }

      title = !loadingSelectedRecord && selectedRecord ? parser(label) : <Skeleton.Input />
    }
    return title
  }

  return <>{getDrawerTitle()}</>
}

export default connect(mapStateToProps)(HeaderComponent)
