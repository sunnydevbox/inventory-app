/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useState } from 'react'
import { Spin, Form, Input } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from '../../../../../../config'

const FormItem = Form.Item

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    // commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    // setFormTouched: formTouched => dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    // setFormReference: formReference => dispatch({ type: `${STORE_PREFIX}/FORM_SET_REFERENCE`, formReference }),
    setSKU: code => dispatch({ type: `${STORE_PREFIX}/PRODUCT_SKU_UPDATE`, code }),
  }
}

const SkuComponent = ({ [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord }, setSKU }) => {
  const [inFocus, setFocus] = useState(false)
  const [form] = Form.useForm()

  const displayN = () => {
    return inFocus ? (
      <div>
        <em>Press enter to save</em>
      </div>
    ) : null
  }

  const getSku = () => {
    if (selectedRecord && !loadingSelectedRecord) {
      return selectedRecord.sku
    }

    return null
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        form={form}
        initialValues={{
          code: '',
        }}
        onFinish={() => {
          setSKU(form.getFieldValue('sku'))
          form.resetFields()
          setFocus(false)
        }}
      >
        <FormItem
          name="sku"
          // rules={[{ required: true }, { min: 3, message: 'Minimum of 3 characters' }]}
        >
          <Input
            autoComplete="off"
            placeholder={getSku()}
            onFocus={() => {
              setFocus(true)
            }}
            onBlur={() => {
              console.log('blur')
              setFocus(false)
            }}
          />
        </FormItem>
        <div>
          <em className="text-muted">
            Leave blank to autogenerate the SKU code. Requires a category to be selected.
          </em>
        </div>
        {displayN()}
        {/* <FormItem
          label="Item Name"
          name="name"
        >
          <Input />
          <em className="text-muted">Leave blank to autogenerate the SKU code. Requires a category to be selected.</em>
        </FormItem> */}
      </Form>
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(SkuComponent)
