/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import { Input, Form, Spin, Button, Radio, InputNumber } from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import SettingsDropdownSimpleComponent from 'components/form/settings-dropdown-simple'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../../../config'

const { TextArea } = Input
const FormItem = Form.Item

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    setFormReference: formReference =>
      dispatch({ type: `${STORE_PREFIX}/FORM_SET_REFERENCE`, formReference }),
  }
}

const TabItemFormComponent = ({
  [STORE_PREFIX]: {
    selectedRecord,
    loadingSelectedRecord,
    drawerState,
    formTriggerSave,
    formReference,
    formSubmissionFailed,
  },
  commitRecord,
  setFormTouched,
  setFormReference,
}) => {
  const [form] = Form.useForm()

  useEffect(() => {
    setFormReference(form)

    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      form.setFieldsValue({
        name: selectedRecord.name,
        description: selectedRecord.description,
        metric_id: selectedRecord.metric_id,
        category_id: selectedRecord.category_id,
        has_expiration: selectedRecord.has_expiration,
        expiration_threshold: selectedRecord.expiration_threshold,
      })
    } else {
      form.setFieldsValue({
        name: form.getFieldValue('name'),
        description: form.getFieldValue('description'),
        metric_id: form.getFieldValue('metric_id'),
        category_id: form.getFieldValue('category_id'),
        has_expiration: form.getFieldValue('has_expiration'),
        expiration_threshold: form.getFieldValue('expiration_threshold'),
      })
    }

    // console.log(formSubmissionFailed)
  }, [
    form,
    setFormReference,
    formReference,
    drawerState,
    selectedRecord,
    loadingSelectedRecord,
    formTriggerSave,
    formSubmissionFailed,
  ])

  const handleSave = async values => {
    commitRecord(values)
  }

  const dropdownChanged = (type, e) => {
    switch (type) {
      case 'metric':
        form.setFieldsValue({
          metric_id: e,
        })
        break

      case 'categories':
        form.setFieldsValue({
          category_id: e,
        })
        break

      default:
        break
    }

    setFormTouched(true)
  }

  const getDropdownValue = type => {
    if (selectedRecord && selectedRecord[type]) {
      return selectedRecord[type]
    }
    return null
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        layout="vertical"
        form={form}
        initialValues={selectedRecord}
        onFinish={handleSave}
        onFieldsChange={() =>
          // changedValues
          {
            setFormTouched(true)
          }
        }
      >
        <FormItem
          label="Item Name"
          name="name"
          rules={[{ required: true }, { min: 3, message: 'Minimum of 3 characters' }]}
        >
          <Input placeholder="Name" />
        </FormItem>

        <FormItem
          label="Description"
          name="description"
          rules={[{ required: true }, { min: 3, message: 'Minimum of 3 characters' }]}
        >
          <TextArea rows={4} />
        </FormItem>

        <div className="row">
          <div className="col-lg-6">
            <FormItem label="Metrics" name="metric_id">
              <SettingsDropdownSimpleComponent
                type="metrics"
                selected={getDropdownValue('metric_id')}
                callback={v => dropdownChanged('metric', v)}
              />
            </FormItem>
          </div>
          <div className="col-lg-6">
            <FormItem label="Category" name="category_id">
              <SettingsDropdownSimpleComponent
                type="categories"
                selected={getDropdownValue('category_id')}
                callback={v => dropdownChanged('categories', v)}
              />
            </FormItem>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-6">
            <FormItem label="Expires?" name="has_expiration" layout="horizontal">
              <Radio.Group
                onChange={e => {
                  console.log(e)
                }}
              >
                <Radio value={1}>Yes</Radio>
                <Radio value={0}>No</Radio>
              </Radio.Group>
            </FormItem>
          </div>

          <div className="col-lg-6">
            <FormItem
              label="Expiration Threshold (in days)"
              name="expiration_threshold"
              layout="horizontal"
            >
              <InputNumber />
            </FormItem>
          </div>
        </div>

        <div>
          <Button type="primary" htmlType="submit" className="cta-btn-edit mr-2">
            Save
          </Button>

          <Button
            type="primary"
            onClick={() => {
              // formCancel()

              form.resetFields()
            }}
          >
            Clear
          </Button>
        </div>
      </Form>
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(TabItemFormComponent)
