/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React from 'react'
import { Avatar, Upload, Spin } from 'antd'
import { UserOutlined } from '@ant-design/icons'
import { connect } from 'react-redux'
import ProductService from 'services/ProductService'
import { STORE_PREFIX } from '../../../../../../config'
// import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const AvatarComponent = ({ [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord } }) => {
  const onChange = file => {
    console.log(selectedRecord, file.file)
    ProductService.imageUpload(selectedRecord.id, file.file)
      .then(res => console.log(res))
      .catch(errx => console.log(errx))
  }

  return (
    <>
      <Spin spinning={loadingSelectedRecord}>
        <Upload maxCount="1" listType="picture" multiple={false} onChange={onChange}>
          <Avatar shape="square" icon={<UserOutlined />} />
        </Upload>
      </Spin>
    </>
  )
}

export default connect(mapStateToProps)(AvatarComponent)
