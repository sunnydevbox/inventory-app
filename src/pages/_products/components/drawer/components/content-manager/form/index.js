/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import { Spin, Tabs } from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import FormModalExitFormComponent from './modal-exit-form'
import TabItemFormComponent from './tabs/items'
import TabPropertiesFormComponent from './tabs/properties'
import TabSupplierFormComponent from './tabs/suppliers'
import AvatarComponent from './avatar'
import SkuComponent from './sku'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../config'

const { TabPane } = Tabs

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    setFormReference: formReference =>
      dispatch({ type: `${STORE_PREFIX}/FORM_SET_REFERENCE`, formReference }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: {
    selectedRecord,
    loadingSelectedRecord,
    drawerState,
    formTriggerSave,
    formReference,
    formSubmissionFailed,
  },
  setFormReference,
}) => {
  // const [currentForms, setCurrentForms] = useState([]);

  useEffect(() => {
    // if (form && !formReference) {
    //   setFormReference(form)
    // }
    // if (drawerState && selectedRecord && !loadingSelectedRecord) {
    //   form.setFieldsValue({
    //     code: selectedRecord.code,
    //   })
    // } else {
    //   form.setFieldsValue({
    //     code: form.getFieldValue('code'),
    //     metric_id: form.getFieldValue('metric_id'),
    //   })
    // }
  }, [
    // form,
    setFormReference,
    formReference,
    drawerState,
    selectedRecord,
    loadingSelectedRecord,
    formTriggerSave,
    formSubmissionFailed,
  ])

  const renderTabBar = (props, DefaultTabBar) => {
    return <DefaultTabBar {...props} className="site-custom-tab-bar" />
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <div className="container">
        <div className="row">
          <div className="col-lg-8">
            <div className="card">
              <div className="card-body">
                <Tabs
                  defaultActiveKey="tab-item"
                  type="card"
                  size="small"
                  renderTabBar={renderTabBar}
                  onChange={e => {
                    console.log(e, TabItemFormComponent)
                  }}
                >
                  <TabPane tab="Item" key="tab-item">
                    <TabItemFormComponent />
                  </TabPane>
                  <TabPane tab="Properites" key="tab-properties">
                    <TabPropertiesFormComponent />
                  </TabPane>
                  <TabPane tab="Suppliers" key="tab-suppliers">
                    <TabSupplierFormComponent />
                  </TabPane>
                  {/* <TabPane tab="Attributes" key="tab-attributes">
                    Attributes
                  </TabPane> */}
                </Tabs>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">SKU</h5>
                <SkuComponent />
              </div>
            </div>

            <div className="card">
              <div className="card-body">
                <AvatarComponent />
              </div>
            </div>
          </div>
        </div>
      </div>
      <FormModalExitFormComponent />
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
