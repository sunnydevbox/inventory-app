import React from 'react'
import { Skeleton } from 'antd'
import { connect } from 'react-redux'
// import { renderField } from 'services/helpers'
import parser from 'html-react-parser'
import ViewRecordDetailsChartComponent from './chart'
import ViewRecordDetailsImageComponent from './images'
import ViewRecordDetailsChartStockMovementComponent from './chart-stock-movement'
import ViewRecordDetailsQuickLastStockMovementComponent from './quick-last-stock-movement'
// import ViewRecordDetailsChartItemStockCountPerLocationComponent from './chart-item-stock-count-per-location'
import { STORE_PREFIX } from '../../../../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const ViewRecordDetailsComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
}) => {
  const getSKU = () => {
    let d = '<em>Not Set</em>'
    if (selectedRecord) {
      if (selectedRecord) {
        const { sku } = selectedRecord
        if (sku) {
          d = sku
        }
      }
    }

    return parser(d)
  }

  const getStockLevel = () => {
    if (selectedRecord) {
      const { stock_count: stockCount } = selectedRecord
      if (stockCount) {
        return stockCount
      }
    }

    return 0
  }

  const getCategory = () => {
    let d = '<em>Not Set</em>'
    if (selectedRecord) {
      if (selectedRecord && selectedRecord.category) {
        const { name } = selectedRecord.category
        if (name) {
          d = name
        }
      }
    }

    return parser(d)
  }

  const getExpiration = () => {
    if (selectedRecord) {
      if (selectedRecord.has_expiration) {
        return `Yes | ${selectedRecord.expiration_threshold}`
      }
    }

    return 'No'
  }

  return (
    <>
      <div className="container pt-3">
        {loadingSelectedRecord || !selectedRecord || !selectedRecord.id ? (
          <Skeleton active />
        ) : (
          <>
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-6">
                    <div className="row">
                      <div className="col-lg-4">
                        <ViewRecordDetailsImageComponent />
                      </div>
                      <div className="col-lg-8">
                        <h2>{selectedRecord.name}</h2>

                        {selectedRecord.description ? (
                          <div className="mb-2">{selectedRecord.description}</div>
                        ) : null}

                        <div>SKU: {getSKU()}</div>
                        <div>Category: {getCategory()}</div>
                        <div>
                          Stock Level: {getStockLevel()} / {selectedRecord.stock_level_threshold}
                        </div>
                        <div>Expires: {getExpiration()}</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="card mb-0">
                      <div className="card-body">
                        <ViewRecordDetailsChartComponent />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-6">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">Stock Count Per Location</h5>
                    {/* <ViewRecordDetailsChartItemStockCountPerLocationComponent /> */}
                  </div>
                </div>
              </div>

              <div className="col-lg-6">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">Latest Movements</h5>
                    <ViewRecordDetailsQuickLastStockMovementComponent />
                  </div>
                </div>
              </div>
            </div>

            <div className="card">
              <div className="card-body">
                <h5 className="card-title">Stock Movement</h5>
                <ViewRecordDetailsChartStockMovementComponent />
              </div>
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default connect(mapStateToProps)(ViewRecordDetailsComponent)
