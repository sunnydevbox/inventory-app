import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import Authorize from 'components/LayoutComponents/Authorize'
import FormComponent from './form'
import ViewRecordDetailsComponent from './view-record-details'
import { STORE_PREFIX } from '../../../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
  }
}

const DrawerContentManagerComponent = ({
  [STORE_PREFIX]: { drawerState, selectedRecord, viewMode },
}) => {
  useEffect(() => {}, [drawerState, selectedRecord])

  return (
    <>
      {viewMode === 'view' ? (
        <Authorize permissions={['products.read']}>
          <ViewRecordDetailsComponent />
        </Authorize>
      ) : (
        ''
      )}
      {viewMode === 'edit' || viewMode === 'new' ? (
        <Authorize permissions={['products.edit']}>
          <FormComponent />
        </Authorize>
      ) : (
        ''
      )}
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContentManagerComponent)
