import React from 'react'
import { Drawer } from 'antd'
import { connect } from 'react-redux'
import DrawerContentManagerComponent from './components/content-manager'
import FooterButtonsComponent from './components/footer-buttons'
import HeaderComponent from './components/header'
import { DRAWER_WIDTH, STORE_PREFIX, DRAWER_PLACEMENT } from '../../config'
import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
    setContentViewMode: viewMode =>
      dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
  }
}

const DrawerComponent = ({ [STORE_PREFIX]: { drawerState }, formCancel }) => {
  return (
    <>
      <Drawer
        title={<HeaderComponent />}
        placement={DRAWER_PLACEMENT}
        height={DRAWER_WIDTH}
        closable={false}
        onClose={() => {
          formCancel()
        }}
        visible={drawerState}
        footer={<FooterButtonsComponent />}
      >
        <DrawerContentManagerComponent />
      </Drawer>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerComponent)
