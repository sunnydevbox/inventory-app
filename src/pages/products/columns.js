import parser from 'html-react-parser'

const columns = [
  {
    title: 'SKU',
    dataIndex: 'sku',
    key: 'sku',
    render: sku => {
      if (!sku) {
        return parser('<em>Not Set</em>')
      }
      return sku
    },
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: name => {
      return name
    },
  },
  {
    title: 'Supplier',
    dataIndex: 'suppliers',
    key: 'suppliers',
    render: suppliers => {
      if (suppliers && suppliers.length) {
        const s = []
        suppliers.forEach(supplier => {
          s.push(`<div>${supplier.name}</div>`)
        })

        return parser(s.join(''))
      }
      return parser('<em>Not Set</em>')
    },
  },

  // {
  //   title: 'Warehouse',
  //   dataIndex: 'location',
  //   key: 'warehouse',
  //   render: row => {
  //     return row.name
  //   },
  // }

  // {
  //   title: 'Metric',
  //   dataIndex: 'metric',
  //   key: 'metric',
  //   render: metric => {
  //     if (metric) {
  //       return `${metric.name} (${metric.symbol})`
  //     }
  //     return parser('<em>Not Set</em>')
  //   },
  // },

  {
    title: 'Category',
    dataIndex: 'category',
    key: 'category',
    render: category => {
      if (category) {
        return category.name
      }
      return parser('<em>Not Set</em>')
    },
  },

  {
    title: 'Stocks',
    dataIndex: 'stock_count',
    key: 'stock_count',
    render: stockCount => {
      if (stockCount) {
        return stockCount
      }
      return parser('<em>0</em>')
    },
  },

  {
    title: 'Expires?',
    dataIndex: 'has_expiration',
    key: 'has_expiration',
    render: expires => {
      return expires ? 'Yes' : 'No'
      // if ()
      // return parser('<em>Not Set</em>')
    },
  },

]

export default columns
