/* V1.0 */
import Service from 'services/ProductService'

export const LABEL_SINGULAR = 'Product'
export const LABEL_PLURAL = 'Products'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featureBread'
export const API_SERVICE = Service
