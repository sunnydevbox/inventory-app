/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useCallback, useEffect, useState } from 'react'
import { Spin, Form, Input } from 'antd'
import { connect } from 'react-redux'
import ProductService from 'services/ProductService'
import { STORE_PREFIX } from 'pages/products/config'

const FormItem = Form.Item

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const SkuComponent = ({
  dispatch,
  [STORE_PREFIX]: {
    selectedRecord, loadingSelectedRecord
  },
}) => {
  const [inFocus, setFocus] = useState(false)
  const [form] = Form.useForm()

  const selectRecord = useCallback(id => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id }), [dispatch])
  const getList = useCallback(() => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST` }), [dispatch])

  useEffect(() => {

  }, [
    
  ])

  const displayN = () => {
    return inFocus ? (
      <div>
        <em>Press enter to save</em>
      </div>
    ) : null
  }

  const getSku = () => {
    if (selectedRecord && !loadingSelectedRecord) {
      return selectedRecord.sku
    }

    return null
  }

  const setSKU = (v) => {

    console.log(v, selectedRecord)

    // inventoryId, code)
    ProductService.skuUpdate(selectedRecord.id, v)
      .then(res => {
        console.log(res)
        selectRecord(selectedRecord.id)
        getList()
      })
      .catch(err => console.log(err))

    /*
      yield call(SELECT_RECORD, { id: selectedRecord.id })
      yield call(API_GET_LIST)
    */
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        form={form}
        initialValues={{
          code: '',
        }}
        onFinish={() => {
          setSKU(form.getFieldValue('sku'))
          form.resetFields()
          setFocus(false)
        }}
      >
        <FormItem
          name="sku"
          // rules={[{ required: true }, { min: 3, message: 'Minimum of 3 characters' }]}
          className="mb-0"
        >
          <Input
            autoComplete="off"
            disabled={!selectedRecord || !selectedRecord.category_id}
            placeholder={getSku()}
            onFocus={() => {
              setFocus(true)
            }}
            onBlur={() => {
              console.log('blur')
              setFocus(false)
            }}
          />
        </FormItem>
        <div>
          <em className="text-muted small">
            Leave blank to autogenerate the SKU code. Requires a category to be selected.
          </em>
        </div>
        {displayN()}
        {/* <FormItem
          label="Item Name"
          name="name"
        >
          <Input />
          <em className="text-muted">Leave blank to autogenerate the SKU code. Requires a category to be selected.</em>
        </FormItem> */}
      </Form>
    </Spin>
  )
}

export default connect(mapStateToProps)(SkuComponent)
