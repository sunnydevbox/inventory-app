/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import { Carousel, Spin } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from 'pages/products/config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    setFormTouched: formTouched =>
      dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    setFormReference: formReference =>
      dispatch({ type: `${STORE_PREFIX}/FORM_SET_REFERENCE`, formReference }),
  }
}

const contentStyle = {
  height: '160px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
}

const ImageCarouselComponent = ({ [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord } }) => {
  useEffect(() => {
    // console.log(formSubmissionFailed)
  }, [selectedRecord])

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Carousel autoplay>
        <div>
          <h3 style={contentStyle}>1</h3>
        </div>
        <div>
          <h3 style={contentStyle}>2</h3>
        </div>
        <div>
          <h3 style={contentStyle}>3</h3>
        </div>
        <div>
          <h3 style={contentStyle}>4</h3>
        </div>
      </Carousel>
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(ImageCarouselComponent)
