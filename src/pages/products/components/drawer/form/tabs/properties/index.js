/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import { Form, Spin, Button, InputNumber } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from 'pages/products/config'

const FormItem = Form.Item

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    setFormTouched: formTouched => dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    setFormReference: formReference => dispatch({ type: `${STORE_PREFIX}/FORM_SET_REFERENCE`, formReference }),
  }
}

const TabPropertiesFormComponent = ({
  [STORE_PREFIX]: {
    selectedRecord,
    loadingSelectedRecord,
    drawerState,
    formTriggerSave,
    formReference,
    formSubmissionFailed,
  },
  commitRecord,
  setFormTouched,
  setFormReference,
}) => {
  const [form] = Form.useForm()

  useEffect(() => {
    if (form && !formReference) {
      console.log(form)
      setFormReference(form)
    }
    console.log(drawerState, selectedRecord)
    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      
      form.setFieldsValue({
        width: selectedRecord.width,
        height: selectedRecord.height,
        length: selectedRecord.length,
        depth: selectedRecord.depth,
        area: selectedRecord.area,
      })
    } else {
      form.setFieldsValue({
        width: form.getFieldValue('width'),
        height: form.getFieldValue('height'),
        length: form.getFieldValue('length'),
        area: form.getFieldValue('area'),
      })
    }

    // console.log(formSubmissionFailed)
  }, [
    form,
    setFormReference,
    formReference,
    drawerState,
    selectedRecord,
    loadingSelectedRecord,
    formTriggerSave,
    formSubmissionFailed,
  ])

  const handleSave = async values => {
    commitRecord(values)
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        layout="vertical"
        form={form}
        initialValues={selectedRecord}
        onFinish={handleSave}
        onFieldsChange={() =>
          // changedValues
          {
            setFormTouched(true)
          }
        }
      >
        <div className="row">
          <div className="col-lg-3">
            <FormItem label="Width (in cm)" name="width" rules={[]}>
              <InputNumber min={0} />
            </FormItem>
          </div>
          <div className="col-lg-3">
            <FormItem label="Height (in cm)" name="height" rules={[]}>
              <InputNumber min={0} />
            </FormItem>
          </div>
          <div className="col-lg-3">
            <FormItem label="Length (in cm)" name="length" rules={[]}>
              <InputNumber min={0} />
            </FormItem>
          </div>
          <div className="col-lg-3">
            <FormItem label="Area" name="area" rules={[]}>
              <InputNumber min={0} />
            </FormItem>
          </div>
        </div>

        <div>
          <Button type="primary" htmlType="submit" className="cta-btn-edit mr-2">
            Save
          </Button>

          <Button
            type="primary"
            onClick={() => {
              // formCancel()

              form.resetFields()
            }}
          >
            Clear
          </Button>
        </div>
      </Form>
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(TabPropertiesFormComponent)
