/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import { Form, InputNumber } from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import { STORE_PREFIX } from 'pages/products/config'

const FormItem = Form.Item

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    setFormTouched: formTouched => dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
  }
}

const TabItemPricingFormComponent = ({
  [STORE_PREFIX]: {
    selectedRecord,
    loadingSelectedRecord,
    drawerState,
    formTriggerSave,
    formReference,
    formSubmissionFailed,
  },
}) => {

  useEffect(() => {
    if (formReference) {
      if (drawerState && selectedRecord && !loadingSelectedRecord) {
        formReference.setFieldsValue({
          last_purchase_price: selectedRecord.last_purchase_price,
        })
      } else {
        formReference.setFieldsValue({
          last_purchase_price: formReference.getFieldValue('last_purchase_price'),
        })
      }
    }
  }, [
    formReference,
    drawerState,
    selectedRecord,
    loadingSelectedRecord,
    formTriggerSave,
    formSubmissionFailed,
  ])

  return (
    <>
      <FormItem
        label="Last Purchase Price"
        name="last_purchase_price"
      >
        <InputNumber min={0} />
      </FormItem>

      {/* <FormItem
        label="Description"
        name="description"
        rules={[{ required: true }, { min: 3, message: 'Minimum of 3 characters' }]}
      >
        <TextArea rows={4} />
      </FormItem> */}
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(TabItemPricingFormComponent)
