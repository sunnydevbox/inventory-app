/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from 'react'
import {
  Input, Form, Spin,
  // Button, 
  // Radio,
  InputNumber
} from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import SettingsDropdownSimpleComponent from 'components/form/settings-dropdown-simple'
import { STORE_PREFIX } from 'pages/products/config'
// import TabItemPricingFormComponent from './pricing'
import './styles.scss'

const { TextArea } = Input
const FormItem = Form.Item

const initV = {
  name: '',
  description: '',
  metric_id: '',
  category_id: '',
  has_expiration: false,
  expiration_threshold: 0,
  brand: '',
  stock_alert_level: 0,
}

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    setFormTouched: formTouched => dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    setFormReference: formReference => dispatch({ type: `${STORE_PREFIX}/FORM_SET_REFERENCE`, formReference }),
  }
}

const TabItemFormComponent = ({
  [STORE_PREFIX]: {
    selectedRecord,
    loadingSelectedRecord,
    drawerState,
    formTriggerSave,
    formReference,
    formSubmissionFailed,
  },
  // commitRecord,
  setFormTouched,
  setFormReference,
}) => {
  const [form] = Form.useForm()

  const [selectedCategory, setSelectedCategory] = useState(null)
  const [selectedMetric, setSelectedMetric] = useState(null)




  useEffect(() => {
    setFormReference(form)
    if (drawerState) {
      if (selectedRecord && !loadingSelectedRecord) {

        form.setFieldsValue({
          name: selectedRecord.name,
          description: selectedRecord.description,
          metric_id: selectedCategory ?? selectedRecord.metric_id,
          category_id: selectedCategory ?? selectedRecord.category_id,
          has_expiration: selectedRecord.has_expiration,
          expiration_threshold: selectedRecord.expiration_threshold,
          brand: selectedRecord.brand,
          stock_alert_level: selectedRecord.stock_alert_level,
        })
        // setSelectedCategory(selectedRecord.category_id)
        if (!selectedCategory) {
          setSelectedCategory(selectedRecord.metric_id)
        }

        if (!selectedMetric) {
          setSelectedMetric(selectedRecord.metric_id)
        }
      }
    } else {
      // EMPTY THE FORM ON EXIT
      form.setFieldsValue(initV)
      setSelectedCategory(null)
      setSelectedMetric(null)
    }
  }, [
    form,
    selectedCategory,
    selectedMetric,
    setFormReference,
    formReference,
    drawerState,
    selectedRecord,
    loadingSelectedRecord,
    formTriggerSave,
    formSubmissionFailed,
    setSelectedCategory,
    setSelectedMetric,
  ])

  // const handleSave = async values => {
  //   commitRecord(values)
  // }

  const dropdownChanged = (type, e) => {
    switch (type) {
      case 'metric':
        form.setFieldsValue({
          metric_id: e,
        })
        break

      case 'categories':
        form.setFieldsValue({
          category_id: e,
        })
        setSelectedCategory(e)
        break

      default:
        break
    }

    setFormTouched(true)
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        layout="vertical"
        form={form}
        initialValues={initV}
        // onFinish={handleSave}
        onFieldsChange={() =>
        // changedValues
        {
          setFormTouched(true)
        }
        }
      >
        <div className="row">
          <div className="col-sm-8">
            <FormItem
              label="Item Name"
              name="name"
              rules={[{ required: true }, { min: 3, message: 'Minimum of 3 characters' }]}
            >
              <Input />
            </FormItem>

            <FormItem
              label="Description"
              name="description"
              rules={[{ required: true }, { min: 3, message: 'Minimum of 3 characters' }]}
            >
              <TextArea rows={4} />
            </FormItem>
          </div>
          <div className="col-sm-4">
            <FormItem label="Brand" name="brand">
              <Input />
            </FormItem>

            <FormItem label="Category" name="category_id">
              <SettingsDropdownSimpleComponent
                type="categories"
                selected={selectedCategory}
                callback={v => dropdownChanged('categories', v)}
              />
            </FormItem>

            <FormItem label="Metrics" name="metric_id">
              <SettingsDropdownSimpleComponent
                type="metrics"
                selected={selectedMetric}
                callback={v => dropdownChanged('metric', v)}
              />
            </FormItem>
          </div>
        </div>



        {/* <TabItemPricingFormComponent /> */}

        <div className="row">
          {/* <div className="col-lg-6">
            <FormItem label="Expires?" name="has_expiration" layout="horizontal">
              <Radio.Group
                onChange={e => {
                  console.log(e)
                }}
              >
                <Radio value={1}>Yes</Radio>
                <Radio value={0}>No</Radio>
              </Radio.Group>
            </FormItem>
          </div> */}
          <div className="col-lg-6">
            <FormItem
              label="Stock Level Alert"
              name="stock_alert_level"
              layout="horizontal"
            >
              <InputNumber min={0} />
            </FormItem>
          </div>

          <div className="col-lg-6">
            <FormItem
              label="Expiration Threshold (in days)"
              name="expiration_threshold"
              layout="horizontal"
            >
              <InputNumber min={0} />
            </FormItem>
          </div>
        </div>

        {/* <div>
          <Button type="primary" htmlType="submit" className="cta-btn-edit mr-2">
            Save
          </Button>

          <Button
            type="primary"
            onClick={() => {
              // formCancel()

              form.resetFields()
            }}
          >
            Clear
          </Button>
        </div> */}
      </Form>
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(TabItemFormComponent)
