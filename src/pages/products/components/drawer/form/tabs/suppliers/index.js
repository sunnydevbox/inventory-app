/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useCallback, useEffect, useState } from 'react'
import {
  // Form,
  Button,
  Modal,
  Spin,
  Table,
  Form,
  InputNumber,
} from 'antd'
import { connect } from 'react-redux'
import parser from 'html-react-parser'
import { STORE_PREFIX } from 'pages/products/config'
import SettingsSupplierDropdown from 'components/form/settings-dropdown-simple'
import ProductService from 'services/ProductService'
import { ExclamationCircleOutlined, PlusCircleOutlined } from '@ant-design/icons'
import './styles.scss'

const { confirm } = Modal
const mapStateToProps = (state) => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = null // dispatch => { return { } }

const TabSupplierFormComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
  ...props
}) => {
  const [form] = Form.useForm()

  const [firstLoad, setFirstLoad] = useState(true)
  const [modalVisibility, setModalVisibility] = useState(false)
  const [selectedSupplier, setSelectedSupplier] = useState(null)
  const [isLoading, setIsLoading] = useState(false)
  const [dataSource, setData] = useState([])
  const [viewMode, setViewMode] = useState(null)
  const [selectedSupplierName, setSelectedSupplierName] = useState(null)

  const columns = [
    {
      title: 'Supplier',
      dataIndex: 'name',
      key: 'supplier',
      className: 'text-right bg-transparent text-gray-6',
    },
    {
      title: 'Last Purchase Price',
      dataIndex: 'last_purchase_price',
      key: 'last_purchase_price',
      className: 'text-right bg-transparent text-gray-6',
    },
    {
      title: 'Current Stock',
      dataIndex: 'latest_stock',
      key: 'latest_stock',
      className: 'text-right bg-transparent text-gray-6',
    },
    {
      className: 'text-right bg-transparent text-gray-6',
      render: (record) => {
        return (
          <>
            <a
              className="btn btn-sm btn-light mr-2"
              href="#"
              onClick={() => {
                setSelectedSupplier(record.id)
                setSelectedSupplierName(record.name)
                setViewMode('edit')
                form.setFieldsValue({ last_purchase_price: record.last_purchase_price })
                toggleModal()
              }}
            >
              <i className="fe fe-edit mr-2" />
              Edit
            </a>

            <a
              className="btn btn-sm btn-warning mr-2"
              href="#"
              onClick={() => {
                confirm({
                  okText: 'Yes',
                  cancelText: 'No',
                  title: `Do you want to delete "${record.name}"?`,
                  icon: <ExclamationCircleOutlined />,
                  content: parser(
                    "Removing this record will also remove it's associated data. <br /><br />Continue?",
                  ),
                  onOk() {
                    deleteSupplier(record.id)
                  },
                })
              }}
            >
              <i className="fe fe-trash mr-2" />
              Remove
            </a>
          </>
        )
      },
    },
  ]

  const toggleModal = useCallback(() => {
    const s = !modalVisibility
    if (!s) {
      setSelectedSupplier(null)
      setSelectedSupplierName(null)
      form.setFieldsValue({ last_purchase_price: 0 })
    }
    setModalVisibility(s)
  }, [form, modalVisibility, setModalVisibility])

  const getList = useCallback(() => {
    setIsLoading(true)
    ProductService.browseSuppliers(selectedRecord.id)
      .then((res) => {
        setData(res.data)
        setIsLoading(false)
      })
      .catch((err) => {
        console.log(err)
        setIsLoading(false)
      })
  }, [selectedRecord])

  const deleteSupplier = useCallback(
    (supplierId) => {
      setIsLoading(true)
      ProductService.removeSupplier(selectedRecord.id, supplierId)
        .then((res) => {
          setIsLoading(false)
          console.log(res)
          getList()
        })
        .catch((err) => {
          console.log(err)
          setIsLoading(false)
        })
    },
    [selectedRecord, getList],
  )

  const addSupplier = useCallback(() => {
    setIsLoading(true)
    ProductService.addSupplier(
      selectedRecord.id,
      selectedSupplier,
      form.getFieldValue('last_purchase_price'),
    )
      .then(() => {
        setIsLoading(false)
        getList()
        toggleModal()

        form.setFieldsValue({ last_purchase_price: 0 })
        setSelectedSupplier(null)
      })
      .catch((err) => {
        setIsLoading(false)
        console.log(err)
      })
  }, [form, selectedRecord, selectedSupplier, getList, toggleModal])

  const editSupplier = useCallback(() => {
    setIsLoading(true)
    ProductService.editSupplier(
      selectedRecord.id,
      selectedSupplier,
      form.getFieldValue('last_purchase_price'),
    )
      .then(() => {
        setIsLoading(false)
        getList()
        toggleModal()

        form.setFieldsValue({ last_purchase_price: 0 })
        setSelectedSupplier(null)
      })
      .catch((err) => {
        setIsLoading(false)
        console.log(err)
      })
  }, [form, selectedRecord, selectedSupplier, getList, toggleModal])

  useEffect(() => {
    if (firstLoad) {
      setFirstLoad(false)
      getList()
    }
  }, [firstLoad, props, getList])

  const getSupplierField = () => {
    if (viewMode === 'new') {
      return (
        <>
          <SettingsSupplierDropdown
            type="suppliers"
            placeholder="Select Supplier"
            selected={selectedSupplier}
            callback={(v) => {
              setSelectedSupplier(v)
              form.setFieldsValue({
                supplier_id: v,
              })
            }}
          />
        </>
      )
    }

    if (viewMode === 'edit') {
      return selectedSupplierName
    }

    return null
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Table
        loading={isLoading}
        columns={columns}
        dataSource={dataSource}
        pagination={false}
        rowKey={() => {
          // console.log(index)
          return Math.random() * 100 + 1
          // return row.id + index
        }}
      />

      <Button
        className="btn-add"
        onClick={() => {
          setViewMode('new')
          toggleModal()
        }}
      >
        <PlusCircleOutlined /> Add Supplier
      </Button>

      <Modal
        visible={modalVisibility}
        okText={viewMode === 'new' ? 'Add' : 'Update'}
        onCancel={() => {
          setModalVisibility(false)
        }}
        onOk={() => {
          if (viewMode === 'new') {
            addSupplier()
          } else {
            editSupplier()
          }
        }}
      >
        <Form
          form={form}
          layout="vertical"
          initialValues={{
            last_purchase_price: 0,
          }}
        >
          <Form.Item label="Supplier" name="supplier_id" rules={[{ required: true }]}>
            {getSupplierField()}
          </Form.Item>
          <Form.Item
            label="Initial Price"
            name="last_purchase_price"
            help="Set the initial purchase price."
          >
            <InputNumber min={0} />
          </Form.Item>
        </Form>
      </Modal>
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(TabSupplierFormComponent)
