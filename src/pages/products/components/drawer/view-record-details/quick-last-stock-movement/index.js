import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Table } from 'antd'
import ProductService from 'services/ProductService'
import { STORE_PREFIX } from 'pages/products/config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const ViewRecordDetailsQuickLastStockMovementComponent = ({
  [STORE_PREFIX]: { selectedRecord },
}) => {
  const [hasLoaded, setLoaded] = useState(false)
  const [dataList, setDataList] = useState([])

  const columns = [
    {
      title: 'Date',
      dataIndex: 'created_at',
      key: 'created_at',
      render: createdAt => {
        return createdAt
      },
    },
    {
      title: 'Log',
      dataIndex: 'reason',
      key: 'reason',
      render: createdAt => {
        return createdAt
      },
    },
    {
      title: 'User',
      dataIndex: 'user',
      key: 'user',
      render: user => {
        if (user && user.first_name && user.last_name) {
          return `${user.first_name} ${user.last_name}`
        }

        return null
      },
    },
  ]

  useEffect(() => {
    if (selectedRecord && !hasLoaded) {
      setLoaded(false)
      ProductService.quickLatestStockMovement(selectedRecord.id)
        .then(res => {
          console.log(res)
          setLoaded(true)
          setDataList(res.data)
        })
        .catch(err => {
          console.log(err)
          setLoaded(false)
        })
    }
  }, [hasLoaded, dataList, selectedRecord])

  return (
    <>
      <Table
        loading={selectedRecord && !hasLoaded}
        dataSource={dataList}
        columns={columns}
        rowKey={row => row.id}
        pagination={false}
      />
    </>
  )
}

export default connect(mapStateToProps)(ViewRecordDetailsQuickLastStockMovementComponent)
