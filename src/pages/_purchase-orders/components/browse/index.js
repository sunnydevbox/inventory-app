import React, { useCallback, useEffect, useState } from 'react'
import {
  Space,
  Table,
  // Button
} from 'antd'
import { connect } from 'react-redux'
import parser from 'html-react-parser'
import { formatDate, poStateTranslation } from 'services/helpers'
import RefreshButtonComponent from 'components/btn-refresh'
import SearchbarComponent from './components/searchbar'
import FiltersComponent from './components/filters'
import './styles.scss'
import { STORE_PREFIX } from '../../config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    toggleDrawer: () => dispatch({ type: `${STORE_PREFIX}/TOGGLE_DRAWER` }),
    getList: () => dispatch({ type: `${STORE_PREFIX}/API_GET_LIST` }),
    setFilter: filters => dispatch({ type: `${STORE_PREFIX}/SET_FILTER`, filters }),
    setRecord: id => dispatch({ type: `${STORE_PREFIX}/SELECT_RECORD`, id }),
  }
}

const FeatureBrowseComponent = ({
  [STORE_PREFIX]: { dataList, listLoading, pagination },
  toggleDrawer,
  setFilter,
  setRecord,
  getList,
}) => {
  const [firstLoad, setFirstLoad] = useState(true)

  const columns = [
    {
      title: 'Code',
      dataIndex: 'code',
      key: 'code',
    },

    {
      title: 'Order Date',
      dataIndex: 'order_date',
      key: 'order_date',
      render: orderDate => {
        return formatDate(orderDate)
      },
    },

    {
      title: 'Supplier',
      dataIndex: 'supplier',
      key: 'supplier',
      render: supplier => {
        if (supplier) {
          return supplier.name
        }
        return parser('<em>Not Set</em>')
      },
    },

    {
      title: 'Items Count',
      dataIndex: 'items_count',
      key: 'items_count',
    },

    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: status => {
        return parser(`<span style="text-transform:uppercase">${poStateTranslation(status)}</span>`)
      }
    },
  ]

  const trig = useCallback(() => {
    setFilter()
  }, [setFilter])

  useEffect(() => {
    if (firstLoad) {
      setFirstLoad(false)
      trig()
    }
  }, [firstLoad, trig])

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <div className="row">
          <div className="col-sm-11">
            <Space>
              <SearchbarComponent />
              <FiltersComponent />
            </Space>
          </div>
          <div className="col-sm-1">
            <RefreshButtonComponent
              style={{ float: 'right' }}
              callback={getList}
              loading={listLoading}
            />
          </div>
        </div>
        <Table
          loading={listLoading}
          dataSource={dataList}
          columns={columns}
          rowKey={row => row.id}
          onChange={trig}
          pagination={{
            showTotal: total => `Total ${total} items`,
            total: pagination.total,
            defaultPageSize: pagination.per_page,
            onChange: (page, pageSize) => {
              console.log('onChange', page, pageSize)
              setFilter({
                page,
                limit: pageSize,
              })
            },
            position: ['bottomCenter'],
          }}
          onRow={record => {
            return {
              onClick: () => {
                toggleDrawer()
                setRecord(record.id)
              },
            }
          }}
        />
      </Space>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FeatureBrowseComponent)
