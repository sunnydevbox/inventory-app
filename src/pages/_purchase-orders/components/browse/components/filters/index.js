import React, { useEffect, useState } from 'react'
import { Space } from 'antd'
import { connect } from 'react-redux'
import SettingsDropdown from 'components/form/settings-dropdown-simple'
import { STORE_PREFIX } from '../../../../config'
import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    setFilter: filters => dispatch({ type: `${STORE_PREFIX}/SET_FILTER`, filters }),
    resetFilters: () => dispatch({ type: `${STORE_PREFIX}/RESET_FILTERS` }),
  }
}

const FiltersComponent = ({ setFilter, resetFilters }) => {
  const [selectedState, setState] = useState('all')
  const [selectedLocation, setLocation] = useState('all')
  const [selectedSupplier, setSupplier] = useState('all')

  useEffect(() => { }, [selectedState])

  const filterChanged = (key, value) => {
    if (key) {
      const d = []
      d[key] = value
      setFilter(d)
    } else {
      resetFilters()
    }

    // if (key === null) {
    //   filters.filters = {}
    // } else {
    //   filters.filters[key] = value
    // }

    // console.log('fileCHanged', key, value, filters.filters)
  }

  const reset = () => {
    setState('all')
    setLocation('all')
    setSupplier('all')
  }

  return (
    <Space>
      <SettingsDropdown
        type="po_states"
        selected={selectedState}
        withAll
        callback={v => {
          setState(v)
          filterChanged('state', v)
        }}
        width="200"
      />

      <SettingsDropdown
        type="suppliers"
        selected={selectedSupplier}
        withAll
        callback={v => {
          setSupplier(v)
          filterChanged('supplier', v)
        }}
      />

      <SettingsDropdown
        type="locations"
        selected={selectedLocation}
        withAll
        callback={v => {
          setLocation(v)
          filterChanged('location', v)
        }}
      />

      <a
        role="button"
        tabIndex={0}
        onClick={() => {
          filterChanged(null)
          reset()
        }}
        onKeyDown={() => filterChanged(null)}
      >
        Clear Filters
      </a>
    </Space>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FiltersComponent)
