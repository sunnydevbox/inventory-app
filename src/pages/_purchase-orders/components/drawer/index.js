import React, { useEffect } from 'react'
import { Drawer, Skeleton } from 'antd'
import { connect } from 'react-redux'
import parser from 'html-react-parser'
import DrawerContentManagerComponent from './components/content-manager'
import FooterButtonsComponent from './components/footer-buttons'
import { LABEL_SINGULAR, DRAWER_WIDTH, STORE_PREFIX, DRAWER_PLACEMENT } from '../../config'
import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
    setContentViewMode: viewMode => dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
  }
}

const DrawerComponent = ({
  [STORE_PREFIX]: { 
    drawerState, 
    selectedRecord, 
    loadingSelectedRecord, 
    viewMode 
  },
  formCancel,
}) => {
  useEffect(() => {}, [drawerState, selectedRecord])

  const getDrawerTitle = () => {
    let title = ''

    if (!selectedRecord && viewMode === 'new') {
      title = `Add New ${LABEL_SINGULAR} Record`
    } else if (selectedRecord && selectedRecord.id) {
      const { code } = selectedRecord
      let label = `${LABEL_SINGULAR}: <strong>${code}</strong>`

      if (viewMode === 'edit') {
        label = `Update ${LABEL_SINGULAR}: <strong>${code}</strong>`
      }

      // label += ` Last Update: ${formatDate(updatedAt, 'LLL')}`

      title = !loadingSelectedRecord && selectedRecord ? parser(label) : <Skeleton.Input />
    }
    return title
  }

  return (
    <>
      <Drawer
        title={getDrawerTitle()}
        placement={DRAWER_PLACEMENT}
        height={DRAWER_WIDTH}
        closable={false}
        onClose={() => {
          formCancel()
        }}
        visible={drawerState}
        footer={<FooterButtonsComponent />}
      >
        <DrawerContentManagerComponent />
      </Drawer>
    </>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerComponent)
