import React, { useEffect } from 'react'
import { Table } from 'antd'
import { connect } from 'react-redux'
import { STORE_PREFIX } from '../../../../../config'
import columns from './columns'
import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const dataSource = [
  {
    key: 'subtotal',
    label: 'Subtotal',
    value: 0,
    editable: false,
  },
  // {
  //   key: 'discount',
  //   label: 'Discount',
  //   value: 0,
  //   editable: true,
  // },
  // {
  //   key: 'subtotallesdiscount',
  //   label: 'Subtotal Less Discount',
  //   value: 0,
  //   editable: false,
  // },
  // {
  //   key: 'taxrate',
  //   label: 'Tax Rate',
  //   value: 0,
  //   editable: false,
  // },
  // {
  //   key: 'totaltax',
  //   label: 'Total Tax',
  //   value: 0,
  //   editable: false,
  // },
  {
    key: 'shipping',
    label: 'Shipping / Handling',
    value: 0,
    editable: true,
  },
  {
    key: 'total',
    label: 'Total',
    value: 0,
    editable: false,
  },
]

const ItemsSubTotalComponent = ({
  [STORE_PREFIX]: {
    selectedRecord,
    viewMode
  }
}) => {
  useEffect(() => {


    if (selectedRecord) {
      dataSource.forEach((item, index) => {
        dataSource[index].value = selectedRecord[item.key]
      })
    }

  }, [selectedRecord])

  const itemsOverlay = () => {
    if (viewMode === 'new') {
      return <div className="overlay" />
    }

    return null
  }

  const popField = (p) => {
    if (p.editable) {
      // alert(1)
    }
  }

  return (
    <>
      {itemsOverlay()}

      <Table
        dataSource={dataSource}
        columns={columns}
        showHeader={false}
        pagination={false}
        onRow={record => {
          return {
            onClick: () => {
              popField(record)
              console.log(record)
              // toggleDrawer()
              // setRecord(record.id)
            },
          }
        }}
      />
    </>
  )
}

export default connect(mapStateToProps)(ItemsSubTotalComponent)
