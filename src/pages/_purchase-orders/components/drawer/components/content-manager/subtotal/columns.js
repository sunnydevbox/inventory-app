const columns = [
  {
    dataIndex: 'label',
    key: 'label',
  },
  {
    dataIndex: 'value',
    key: 'value',
  },
]

export default columns
