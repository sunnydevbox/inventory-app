import React from 'react'
import { Skeleton } from 'antd'
import { connect } from 'react-redux'
import parser from 'html-react-parser'
import { formatDate, poStateTranslation } from 'services/helpers'
import ItemsSubTotalComponent from '../subtotal'
import FormItemsComponent from '../form/items'
import { STORE_PREFIX } from '../../../../../config'
import './styles.scss'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })

const ViewRecordDetailsComponent = ({
  [STORE_PREFIX]: { selectedRecord, loadingSelectedRecord },
}) => {
  const getSupplier = () => {
    if (selectedRecord && selectedRecord.supplier) {
      const { name, 
        // address, city, state, country, zip_code: zipCode 
      } = selectedRecord.supplier
      return parser(`<span>${name}</span>`)
      // <div>${address} ${city} ${state} ${country} ${zipCode}</div>
    }
    return parser('<em>Not Set</em>')
  }

  // const getPaymentTerms_2 = () => {

  //   if (selectedRecord && selectedRecord.payment_terms) {
  //     const a = []
  //     selectedRecord.payment_terms.forEach(row => a.push(`<span>${row.name}</span>`))
  //     return parser(a.join(', '))
  //   }
  //   return parser('<em>Not Set</em>')
  // }
  
  const getPaymentTerm = () => {

    if (selectedRecord && selectedRecord.payment_term) {
      return selectedRecord.payment_term.name
    }

    return parser('<em>Not Set</em>')
  }

  // const getLocation = () => {
  //   if (selectedRecord && selectedRecord.location) {
  //     return selectedRecord.location.name
  //   }
  //   return parser('<em>Not Set</em>')
  // }

  return (
    <>
      <div className="container-fluid pt-3">
        {loadingSelectedRecord || !selectedRecord || !selectedRecord.id ? (
          <Skeleton active />
        ) : (
          <>
            <div className="row mb-4">
              <div className="col-lg-9">
                <div className="row">
                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body">
                        <h6 className={`po-status-indicator ${selectedRecord.status}`}>
                          {poStateTranslation(selectedRecord.status)}
                        </h6>
                        {/* PO # {selectedRecord.code}
                        <br />  */}
                        <strong>PO Date:</strong> {formatDate(selectedRecord.order_date)}
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-lg-12">
                            <div><strong>Supplier:</strong> {getSupplier()}</div>
                          </div>
                        </div>
                        {/* <div>Location: {getLocation()}</div> */}
                        <hr />
                        <div className="row">
                          <div className="col-lg-12">
                            <div><strong>Payment Term:</strong>  {getPaymentTerm()}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-4">
                    <div className="card">
                      <div className="card-body small">
                        {/* <div className="card-title">Summary</div> */}
                        <div>Created: {formatDate(selectedRecord.created_at, 'LLL')}</div>
                        <div>Last Update: {formatDate(selectedRecord.updated_at, 'LLL')}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-12">
                    <div className="card">
                      <div className="card-body">
                        <FormItemsComponent />
                      </div>
                    </div>
                    <div className="card">
                      <div className="card-body">
                        <h6 className="card-title">Notes</h6>
                        {selectedRecord.notes}
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-3">
                <div className="card">
                  <div className="card-body">
                    <ItemsSubTotalComponent />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default connect(mapStateToProps)(ViewRecordDetailsComponent)
