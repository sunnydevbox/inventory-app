/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from 'react'
import { Modal, Form, InputNumber } from 'antd'
import { connect } from 'react-redux'
import { Notification } from 'services/helpers'
import DropdownItemSearch from 'components/form/dropdown-search-data'
import ProductService from 'services/ProductService'
import PurchaseOrderService from 'services/PurchaseOrderService'
import { STORE_PREFIX } from 'pages/purchase-orders/config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
// const mapDispatchToProps = dispatch => {
//   return {
//     formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
//   }
// }

const FormItemsModalAddEditComponent = ({
  [STORE_PREFIX]: { selectedRecord },
  showModal,
  poId,
  currentItem,
  visiblityChanged,
  triggerReload,
}) => {
  const [visibility, setVisibility] = useState(false)
  const [ddData, setDDData] = useState([])
  const [form] = Form.useForm()

  useEffect(() => {
    setVisibility(showModal)

    if (currentItem) {
      const { id, quantity, price } = currentItem
      form.setFieldsValue({
        id,
        quantity,
        price,
      })
    }
  }, [showModal, currentItem, form])

  //   const handleSave = async values => {
  //     commitRecord(values)
  //   }

  const closeModal = () => {
    setVisibility(false)
    visiblityChanged(false)
    form.resetFields()
  }

  const getStockField = () => {
    if (currentItem && currentItem.id) {
      // console.log(current)
      return (
        <Form.Item label="Item" value={currentItem.item.name}>
          {currentItem.item.name}
        </Form.Item>
      )
    }

    return (
      <Form.Item name="stock_id" label="Item" rules={[{ required: true }]}>
        <DropdownItemSearch
          setSelected={v => {
            console.log(v)
            form.setFieldsValue({
              stock_id: v
            })
          }}
          optionsData={ddData}
          // Service={ProductService} 
          callback={(v) => {
            ProductService.dropdownResult(v, selectedRecord.supplier_id)
              .then(response => {
                setDDData(response.data)
              })
              .catch(() => {
                // setFetching(false)
              })
          }}
        />
      </Form.Item>
    )
  }

  /**
   * id
   * stock_id
   * qty
   * unit price
   * amount (calcualted)
   * purchase_order_id
   * notes
   */

  return (
    <>
      <Modal
        visible={visibility}
        title="Item Entry"
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              // form.resetFields()
              console.log(currentItem)

              if (!currentItem) {
                // call API
                PurchaseOrderService.addItem(poId, values)
                  .then(() => {
                    Notification({
                      status: 200,
                      title: 'Item added',
                    })
                    form.resetFields()
                    triggerReload(true)
                    closeModal()
                  })
                  .catch(err => {
                    console.log(err)
                  })
              } else {
                PurchaseOrderService.editItem(currentItem.id, values)
                .then(() => {
                  Notification({
                    status: 200,
                    title: 'Item updated',
                  })
                  form.resetFields()
                  triggerReload(true)
                  closeModal()
                })
                .catch(err => {
                  console.log(err)
                })
              }
            })
            .catch(info => {
              console.log('Validate Failed:', info)
            })
        }}
        onCancel={() => {
          closeModal()
        }}
        okText="Save"
        cancelText="Cancel"
      >
        <Form form={form} layout="vertical">

          {getStockField()}

          <Form.Item name="quantity" label="Quantity" rules={[{ required: true }]}>
            <InputNumber />
          </Form.Item>

          <Form.Item name="price" label="Price" rules={[{ required: true }]}>
            <InputNumber />
          </Form.Item>
        </Form>
      </Modal>
    </>
  )
}

export default connect(mapStateToProps)(FormItemsModalAddEditComponent)
// export default FormItemsModalAddEditComponent
