/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react'
import { Input, Form, Spin, DatePicker, Select } from 'antd'
// import Authorize from 'components/LayoutComponents/Authorize'
import { connect } from 'react-redux'
import moment from 'moment'
import parser from 'html-react-parser'
import SettingsDropdownSimpleComponent from 'components/form/settings-dropdown-simple'
import FormModalExitFormComponent from './modal-exit-form'
import FormItemsComponent from './items'
import ItemsSubTotalComponent from '../subtotal'
import './styles.scss'
import { STORE_PREFIX } from '../../../../../config'

const FormItem = Form.Item
const { TextArea } = Input
const { Option } = Select
const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
const mapDispatchToProps = dispatch => {
  return {
    commitRecord: async data => dispatch({ type: `${STORE_PREFIX}/API_COMMIT_RECORD`, data }),
    setFormTouched: formTouched => dispatch({ type: `${STORE_PREFIX}/SET_FORM_TOUCHED`, formTouched }),
    setFormReference: formReference => dispatch({ type: `${STORE_PREFIX}/FORM_SET_REFERENCE`, formReference }),
    setViewMode: viewMode => dispatch({ type: `${STORE_PREFIX}/SET_CONTENT_VIEW_MODE`, viewMode }),
  }
}

const FormComponent = ({
  [STORE_PREFIX]: {
    selectedRecord,
    loadingSelectedRecord,
    drawerState,
    formTriggerSave,
    formReference,
    viewMode,
  },
  commitRecord,
  setFormTouched,
  setFormReference,
  setViewMode,
}) => {
  const [form] = Form.useForm()
  const dateFormat = 'MMM DD, YYYY'

  useEffect(() => {
    setFormReference(form)

    if (drawerState && selectedRecord && !loadingSelectedRecord) {
      form.setFieldsValue({
        code: selectedRecord.code,
        supplier_id: selectedRecord.supplier_id,
        order_date: selectedRecord.order_date,
      })
    } else {
      form.setFieldsValue({
        code: form.getFieldValue('code'),
        supplier_id: form.getFieldValue('supplier_id'),
        order_date: form.getFieldValue('order_date'),
      })
    }

    if (selectedRecord) {
      setViewMode('edit')
    }
  }, [
    form,
    setFormReference,
    formReference,
    drawerState,
    selectedRecord,
    loadingSelectedRecord,
    formTriggerSave,
    setViewMode,
  ])

  const handleSave = async values => {
    console.log(form.getFieldWarning('code'))
    // SAVE AND THEN LOAD
    commitRecord(values)
  }

  const getDefaultOrderDate = () => {
    if (selectedRecord && selectedRecord.order_date) {
      return moment(selectedRecord.order_date, 'YYYY-MM-DD')
    }

    return null // moment.now()
  }

  const getSupplierField = () => {
    if (viewMode ===  'new') {
      return (
        <FormItem name="supplier_id" label="Supplier" rules={[{ required: true }]}>
          <SettingsDropdownSimpleComponent
            type="suppliers"
            selected={form.getFieldValue('supplier_id')}
            callback={v => {
              form.setFieldsValue({ supplier_id: v })
              setFormTouched(true)
            }}
          />
        </FormItem>
      )
    }
    
    const { supplier } = selectedRecord
    if (supplier) {
      return parser(`<label>Supplier</label><div>${supplier.name}</div>`)
    }

    return null
  }

  const getPaymentTermsField = () => {
    // getPaymentTerms

    if (viewMode ===  'edit') {

      return (
        <FormItem name="payment_term_id" label="Payment Term">
          <Select>
            {selectedRecord.payment_terms.map(row => <Option key={row.id} value={row.id}>{row.name}</Option>)}
          </Select>
        </FormItem>
      )
    }
    
    const { supplier } = selectedRecord
    if (supplier) {
      return parser(`<label>Supplier</label><div>${supplier.name}</div>`)
    }

    return null
  }

  const itemsOverlay = () => {
    if (viewMode === 'new') {
      return <div className="overlay" />
    }

    return null
  }

  const getPOField = () => {
    if (viewMode ===  'new') {
      return (
        <FormItem
          name="code"
          label="PO Code"
          rules={[{ required: true }, { min: 3, message: 'Minimum of 3 characters' }]}
        >
          <Input />
        </FormItem>
      )
    }
        // return parser(`<label>PO Code</label><div>${code}</div>`)
    return null
  }

  return (
    <Spin spinning={loadingSelectedRecord}>
      <Form
        layout="vertical"
        form={form}
        initialValues={selectedRecord}
        onFinish={handleSave}
        onFieldsChange={() => {
          setFormTouched(true)
        }}
      >
        <div className="container-fluid">
          <div className="row d-flex justify-content-center">
            <div className="col-md-3">
              <div className="card">
                <div className="card-body">
                  
                  {getPOField()}

                  <div className="field-order-date">
                    <FormItem name="order_date" label="PO Date" className="mb-0">
                      <Input hidden className="d-none" style={{ display: 'none' }} />
                    </FormItem>
                    <DatePicker
                      defaultPickerValue={moment(moment().format('Y/M/D'), dateFormat)}
                      defaultValue={getDefaultOrderDate()}
                      format={dateFormat}
                      onChange={date => {
                        form.setFieldsValue({ order_date: date ? date.format('YYYY-M-D') : null })
                        setFormTouched(true)
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="card">
                <div className="card-body">
                  {getSupplierField()}
                  <hr />
                  {getPaymentTermsField()}
                </div>
              </div>
            </div>

            {(viewMode === 'edit') ?
              <>
                <div className="col-md-6">
                  <div className="card">
                    <div className="card-body">
                      {itemsOverlay()}
                      <FormItemsComponent />
                    </div>
                  </div>

                  <div className="card">
                    <div className="card-body">
                      <FormItem name="notes" label="Notes">
                        <TextArea rows={3} />
                      </FormItem>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="card">
                    <div className="card-body">
                      <ItemsSubTotalComponent />
                    </div>
                  </div>
                </div>
              </>
              : null
            }
          </div>
        </div>
      </Form>

      <FormModalExitFormComponent />
    </Spin>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent)
