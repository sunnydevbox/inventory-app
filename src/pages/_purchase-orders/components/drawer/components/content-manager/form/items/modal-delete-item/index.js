/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from 'react'
import { Modal } from 'antd'
import { connect } from 'react-redux'
import { Notification } from 'services/helpers'
import PurchaseOrderItemService from 'services/PurchaseOrderItemService'
import { STORE_PREFIX } from 'pages/purchase-orders/config'

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
// const mapDispatchToProps = dispatch => {
//   return {
//     formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
//   }
// }

const FormItemsModalAddEditComponent = ({
  showModal,
  currentItem,
  visiblityChanged,
  triggerReload,
}) => {
  const [visibility, setVisibility] = useState(false)

  useEffect(() => {
    setVisibility(showModal)
  }, [showModal, currentItem])

  //   const handleSave = async values => {
  //     commitRecord(values)
  //   }

  const closeModal = () => {
    setVisibility(false)
    visiblityChanged(false)
  }

  return (
    <>
      <Modal
        visible={visibility}
        title="Removing Item"
        onOk={() => {
          if (currentItem) {
            // call API
            PurchaseOrderItemService.destroy(currentItem.id )
              .then(() => {
                Notification({
                  status: 200,
                  title: 'Item removed',
                })
                triggerReload(true)
                closeModal()
              })
              .catch(err => {
                console.log(err)
              })
          }
        }}
        onCancel={() => {
          closeModal()
        }}
        okText="Remove Item"
        cancelText="Cancel"
      >
        Are you sure you want to remove this item from this P.O.?
      </Modal>
    </>
  )
}

export default connect(mapStateToProps)(FormItemsModalAddEditComponent)
