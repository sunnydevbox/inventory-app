/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from 'react'
import { Modal, Form, InputNumber, Input } from 'antd'
import { connect } from 'react-redux'
import { Notification } from 'services/helpers'
import PurchaseOrderService from 'services/PurchaseOrderService'
import { STORE_PREFIX } from 'pages/purchase-orders/config'
import './styles.scss'

const { TextArea } = Input

const mapStateToProps = state => ({ [STORE_PREFIX]: state[STORE_PREFIX] })
// const mapDispatchToProps = dispatch => {
//   return {
//     formCancel: force => dispatch({ type: `${STORE_PREFIX}/FORM_CANCEL`, force }),
//   }
// }

const FormItemsModalServeComponent = ({
  // [STORE_PREFIX]: { selectedRecord },
  showModal,
  currentItem,
  visiblityChanged,
  triggerReload,
}) => {
  const [visibility, setVisibility] = useState(false)
  const [form] = Form.useForm()

  useEffect(() => {
    setVisibility(showModal)

    if (currentItem) {
      const { id, quantity, price } = currentItem
      form.setFieldsValue({
        id,
        quantity,
        price,
      })
    }
  }, [showModal, currentItem, form])

  //   const handleSave = async values => {
  //     commitRecord(values)
  //   }

  const closeModal = () => {
    setVisibility(false)
    visiblityChanged(false)
    form.resetFields()
  }

  /**
   * id
   * stock_id
   * qty
   * unit price
   * amount (calcualted)
   * purchase_order_id
   * notes
   */

  return (
    <>
      <Modal
        visible={visibility}
        title="Serve"
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              // form.resetFields()
              console.log(values)

              if (currentItem) {
                // call API
                PurchaseOrderService.serve(currentItem.id, values)
                  .then(() => {
                    Notification({
                      status: 200,
                      title: 'Items served',
                    })
                    form.resetFields()
                    triggerReload(true)
                    closeModal()
                  })
                  .catch(error => {
                    console.log(error)
                    Notification(error)
                  })
              }
            })
            .catch(info => {
              console.log('Validate Failed:', info)
            })
        }}
        onCancel={() => {
          closeModal()
        }}
        okText="Serve"
        cancelText="Cancel"
      >
        <Form form={form} layout="vertical">
          <table>
            <tbody width="100%">
              <tr>
                <td>Item</td>
                <td>: {currentItem ? currentItem.item.name : null}</td>
              </tr>
              <tr>
                <td>Ordered Quantity</td>
                <td>: {currentItem ? currentItem.quantity : 0}</td>
              </tr>
              <tr>
                <td>Served Quantity</td>
                <td>: {currentItem ? currentItem.served_quantity : 0}</td>
              </tr>
            </tbody>  
          </table>
          
          <hr />
          <div className="row">
            <div className="col-lg-4 mb-3">
              <Form.Item name="served_quantity" label="To Serve Quantity" rules={[{ required: true }]}>
                <InputNumber />
              </Form.Item>
            </div>
            <div className="col-lg-8">
              <Form.Item name="notes" label="Note" rules={[{ required: true }]}>
                <TextArea />
              </Form.Item>
            </div>
          </div>
        </Form>
      </Modal>
    </>
  )
}

export default connect(mapStateToProps)(FormItemsModalServeComponent)