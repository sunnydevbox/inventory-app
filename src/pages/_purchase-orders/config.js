/* V1.0 */
import Service from 'services/PurchaseOrderService'

export const LABEL_SINGULAR = 'Purchase Order'
export const LABEL_PLURAL = 'Purchase Orders'
export const DRAWER_WIDTH = '90%'
export const DRAWER_PLACEMENT = 'top'
export const STORE_PREFIX = 'featurePurchaseOrder'
export const API_SERVICE = Service
